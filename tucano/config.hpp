#ifndef __CONFIG_H__
#define __CONFIG_H__

//
// These values are automatically set according their cmake variables.
//

#define USE_STB_IMAGE 1

#define TUCANO_SHADERS_DIR "E:/Projects/MDP/faceblending/tucano/effects/shaders/"
#define TUCANO_SAMPLES_DIR "E:/Projects/MDP/faceblending/samples/"
#define TUCANO_ASSETS_DIR  "E:/Projects/MDP/faceblending/samples/assets"
#define TUCANO_MODELS_DIR  "E:/Projects/MDP/faceblending/samples/models"
#define TUCANO_IMAGES_DIR  "E:/Projects/MDP/faceblending/samples/images"

#define TUCANO_MODEL_TOY_OBJ     "E:/Projects/MDP/faceblending/samples/models/toy.obj"
#define TUCANO_MODEL_TOY_PLY     "E:/Projects/MDP/faceblending/samples/models/toy.ply"
#define TUCANO_MODEL_SPHERE_PLY  "E:/Projects/MDP/faceblending/samples/models/sphere.ply"
#define TUCANO_MODEL_BUNNY_PLY   "E:/Projects/MDP/faceblending/samples/models/bunny.ply"

#define TUCANO_IMAGE_CAMELO_JPG   "E:/Projects/MDP/faceblending/samples/images/camelo.jpg"
#define TUCANO_IMAGE_CAMELO_PPM   "E:/Projects/MDP/faceblending/samples/images/camelo.ppm"
#define TUCANO_IMAGE_RANDOM_PPM   "E:/Projects/MDP/faceblending/samples/images/random.ppm"

#define TUCANO_IMAGE_THUMBNAIL_A   "E:/Projects/MDP/faceblending/samples/images/thumb_fig02.png"
#define TUCANO_IMAGE_THUMBNAIL_B   "E:/Projects/MDP/faceblending/samples/images/thumb_fig03.png"
#define TUCANO_IMAGE_THUMBNAIL_C   "E:/Projects/MDP/faceblending/samples/images/thumb_fig04.png"
#define TUCANO_IMAGE_THUMBNAIL_D   "E:/Projects/MDP/faceblending/samples/images/thumb_fig05.png"


#endif // __CONFIG_H__
