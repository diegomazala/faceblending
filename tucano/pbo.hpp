#include <thread>
#include <mutex>

#define USE_STB_IMAGE_TO_SAVE_IMAGE_FILES // comment out if not using stb_image to save images
#ifdef USE_STB_IMAGE_TO_SAVE_IMAGE_FILES
#include "stb_image.h"
#endif

namespace gl
{
	static std::mutex mutex;
	class pbo
	{
	public:

		bool is_created() const { return is_initialized; }

		auto get_color_buffer_size() const { return height * stride; }

		auto get_color_buffer() { return color_buffer; }

		bool create()
		{
			GLint viewport[4] = { 0 };
			glGetIntegerv(GL_VIEWPORT, viewport);

			width = viewport[2];
			height = viewport[3];

			

			stride = channel_count * width;
			//g_PboStride += (g_PboStride % 4) ? (4 - g_PboStride % 4) : 0;

			std::cout << "pbo " << width << " " << height << " " << channel_count << " " << stride << std::endl;

			auto data_size = stride * height;
			color_buffer = new GLubyte[data_size];
			memset(color_buffer, 255, data_size);
			glGenBuffers(2, id);
			glBindBuffer(GL_PIXEL_PACK_BUFFER, id[0]);
			glBufferData(GL_PIXEL_PACK_BUFFER, data_size, 0, GL_STREAM_READ);
			glBindBuffer(GL_PIXEL_PACK_BUFFER, id[1]);
			glBufferData(GL_PIXEL_PACK_BUFFER, data_size, 0, GL_STREAM_READ);

			glBindBuffer(GL_PIXEL_PACK_BUFFER, 0);

			is_initialized = (glGetError() == GL_NO_ERROR);
			std::cout << "pbo " << id[0] << " " << id[1] << " " << is_initialized << std::endl;
			return is_initialized;
		}

		void release()
		{
			glDeleteBuffers(2, id);
			id[0] = id[1] = 0;

			// deallocate frame buffer
			delete[] color_buffer;
			color_buffer = 0;

			is_initialized = false;
		}

		void process()
		{
			if (!color_buffer)
				return;

			static int shift = 0;
			static int index = 0;
			int nextIndex = 0;                  // pbo index used for next frame

			// brightness shift amount
			++shift;
			shift %= 200;

			// increment current index first then get the next index
			// "index" is used to read pixels from a framebuffer to a PBO
			// "nextIndex" is used to process pixels in the other PBO
			index = (index + 1) % 2;
			nextIndex = (index + 1) % 2;

			glReadBuffer(GL_FRONT);


			// read framebuffer ///////////////////////////////
			//t1.start();

			// copy pixels from framebuffer to PBO
			// Use offset instead of pointer.
			// OpenGL should perform asynch DMA transfer, so glReadPixels() will return immediately.
			glBindBuffer(GL_PIXEL_PACK_BUFFER, id[index]);
			glPixelStorei(GL_PACK_ALIGNMENT, 1);
			glReadPixels(0, 0, width, height, pixel_format, GL_UNSIGNED_BYTE, 0);


			// for texture
			// glEnable(texture.target)
			// glActiveTexture(GL_TEXTURE0_ARB)
			// glBindTexture(texture.target, texture.id)
			// glGetTexImage(

			//t1.stop();
			//readTime = t1.getElapsedTimeInMilliSec();
			///////////////////////////////////////////////////

			// process pixel data /////////////////////////////
			//t1.start();

			// map the PBO that contain framebuffer pixels before processing it
			glBindBuffer(GL_PIXEL_PACK_BUFFER, id[nextIndex]);
			GLubyte* src = (GLubyte*)glMapBuffer(GL_PIXEL_PACK_BUFFER, GL_READ_ONLY);
			if (src)
			{
				// change brightness
				memcpy(color_buffer, src, height * stride * sizeof(uint8_t));
				glUnmapBuffer(GL_PIXEL_PACK_BUFFER);        // release pointer to the mapped buffer
			}

			// measure the time reading framebuffer
			//t1.stop();
			//processTime = t1.getElapsedTimeInMilliSec();
			///////////////////////////////////////////////////

			glBindBuffer(GL_PIXEL_PACK_BUFFER, 0);
		}


		void save_image(const char* filename)
		{
#ifdef USE_STB_IMAGE_TO_SAVE_IMAGE_FILES
			std::thread save_img(save_image_file, std::string(filename), width, height, channel_count, stride, color_buffer);
			save_img.detach();
#endif
		}


		static int save_image_file(const std::string filename, int w, int h, int c, int stride, uint8_t* data)
		{
#ifdef USE_STB_IMAGE_TO_SAVE_IMAGE_FILES
			auto data_size = h * stride;
			GLubyte* buffer = new GLubyte[data_size];
			{
				std::lock_guard<std::mutex> guard(mutex);
				memcpy(buffer, data, data_size * sizeof(uint8_t));
			}
			stbi_flip_vertically_on_write(true);
			int success = stbi_write_png(filename.c_str(), w, h, c, buffer, stride);
			delete[] buffer;
			return success;
#else
			return false
#endif
		}


	private:

		int width = 0;
		int height = 0;
		int stride = 0;
		const int channel_count = 3;
		const GLenum pixel_format = GL_RGB;
		GLuint id[2] = { 0, 0 };           // IDs of PBOs
		GLubyte* color_buffer = 0;
		bool  is_initialized = false;
	};
}