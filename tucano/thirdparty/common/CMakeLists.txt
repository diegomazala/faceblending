
###########################################################################
#
# common lib
#
set(STB_IMAGE_INCLUDE_DIR ${CMAKE_CURRENT_SOURCE_DIR})
set(LIB_NAME common)
add_library(${LIB_NAME} INTERFACE)
target_include_directories(${LIB_NAME} INTERFACE ${STB_IMAGE_INCLUDE_DIR})
target_sources(${LIB_NAME} INTERFACE 
    ${STB_IMAGE_INCLUDE_DIR}/timer.h
    ${STB_IMAGE_INCLUDE_DIR}/vector_read_write_binary.h)

