
###########################################################################
#
# stb_image lib
#
set(STB_IMAGE_INCLUDE_DIR ${CMAKE_CURRENT_SOURCE_DIR})
set(LIB_NAME stb_image)
add_library(${LIB_NAME} INTERFACE)
target_include_directories(${LIB_NAME} INTERFACE ${STB_IMAGE_INCLUDE_DIR})
target_sources(${LIB_NAME} INTERFACE 
    ${STB_IMAGE_INCLUDE_DIR}/stb_image.h
    ${STB_IMAGE_INCLUDE_DIR}/stb_image.cpp
    ${STB_IMAGE_INCLUDE_DIR}/stb_image_write.h
    ${STB_IMAGE_INCLUDE_DIR}/stb_image_resize.h)

