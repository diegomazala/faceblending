#define STB_IMAGE_IMPLEMENTATION
#include <thirdparty/stb_image/stb_image.h>

#define STB_IMAGE_WRITE_IMPLEMENTATION
#ifdef MSVC
#define STBI_MSC_SECURE_CRT
#endif
#include "stb_image_write.h"

#define STB_IMAGE_RESIZE_IMPLEMENTATION
#include "stb_image_resize.h"