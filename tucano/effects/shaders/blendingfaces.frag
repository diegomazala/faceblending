#version 430


in GS_OUT {
    vec4 vert;
    vec3 normal_vertex;
    vec3 normal_face;
    vec2 texCoords;
    vec4 color;
} fs_in;


uniform mat4 lightViewMatrix;
uniform mat4 viewMatrix;
uniform vec3 ka;
uniform vec3 kd;
uniform vec3 ks;
uniform float shininess;

uniform bool has_texture;
uniform sampler2D model_texture;

uniform float normal_vert_face_lerp = 1.0f;

out vec4 out_color;

void main(void)
{

    vec3 light_intensity = vec3(1.0);

    vec4 model_color = fs_in.color;
    if (has_texture)
    {
        model_color = texture(model_texture, fs_in.texCoords);
    }
    
    vec3 normvec = normalize(mix(fs_in.normal_vertex, fs_in.normal_face, normal_vert_face_lerp));
    
    vec3 lightDirection = (viewMatrix * inverse(lightViewMatrix) * vec4(0.0, 0.0, 1.0, 0.0)).xyz;
    lightDirection = normalize(lightDirection);
    vec3 lightReflection = reflect(-lightDirection, normvec);
    vec3 eyeDirection = normalize(-fs_in.vert.xyz);

    vec3 ambient = light_intensity * ka;
    vec3 diffuse = light_intensity * kd * max(dot(lightDirection, normvec),0.0);
    vec3 specular = light_intensity * ks *  max(pow(dot(lightReflection, eyeDirection), shininess),0.0);

    vec4 norm_color = vec4(normalize((normvec + vec3(1, 1, 1)) * 0.5), 1);
    out_color = model_color * vec4(ambient.xyz + diffuse.xyz + specular.xyz, 1);
    //out_color = norm_color * vec4(ambient.xyz + diffuse.xyz + specular.xyz, 1);
    //out_color = fs_in.color;

}
