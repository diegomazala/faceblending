#version 430

#define MAX_NUMBER_OF_FACES 16

layout ( std430, binding=0 ) buffer vert_attr_t
{
    float vert_data[];
};

in vec4 in_Position;
in vec3 in_Normal;
in vec2 in_TexCoords;


out VS_OUT {
	vec4 vert;
	vec3 normal;
	vec2 texCoords;
	vec4 color;
} vs_out;

uniform int mesh_count = 4;
uniform int vert_count = 0;

const int coeff_order = 3 + 1;

uniform vec4 default_color;
uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;

uniform int base_mesh = 0;

uniform vec2 mouse_pos[3];

uniform vec2 pos2d[MAX_NUMBER_OF_FACES];

uniform vec3 mask_weight;
uniform sampler2D mask_texture;
uniform bool use_mask_color = true;

uniform bool use_curve_interpolation = true;

uniform bool invert_texture_y = false;

uniform float displacement_multiplier = 1.0f;

uniform vec3 color_diff_multiplier;

const float epsilon = 0.001f;
const float pow_distance = 2.0f;

vec3 get_rgb()
{
    int vert_idx = 
         ((2 * mesh_count) * vert_count * 3)    // vertices
        + (2 * (vert_count * coeff_order * 3))  // poly
        + (gl_VertexID * 3);

    return vec3(vert_data[vert_idx + 0], vert_data[vert_idx + 1], vert_data[vert_idx + 2]);
}

vec3 get_vertex_curve_fit(int mesh_index, float mix_value)
{
    int vert_idx;
    // x
    vert_idx = ((2 * mesh_count) * vert_count * 3) + (gl_VertexID * coeff_order) + (vert_count * coeff_order) * (mesh_index * 3 + 0);
    vec4 coeff_x = vec4(vert_data[vert_idx + 0], vert_data[vert_idx + 1], vert_data[vert_idx + 2], vert_data[vert_idx + 3]);
    // y
    vert_idx = ((2 * mesh_count) * vert_count * 3) + (gl_VertexID * coeff_order) + (vert_count * coeff_order) * (mesh_index * 3 + 1);
    vec4 coeff_y = vec4(vert_data[vert_idx + 0], vert_data[vert_idx + 1], vert_data[vert_idx + 2], vert_data[vert_idx + 3]);
    // z
    vert_idx = ((2 * mesh_count) * vert_count * 3) + (gl_VertexID * coeff_order) + (vert_count * coeff_order) * (mesh_index * 3 + 2);
    vec4 coeff_z = vec4(vert_data[vert_idx + 0], vert_data[vert_idx + 1], vert_data[vert_idx + 2], vert_data[vert_idx + 3]);

    float x = mix_value;

    return vec3(
        coeff_x.w * pow(x, 3) + coeff_x.z * pow(x, 2) + coeff_x.y * x + coeff_x.x,
        coeff_y.w * pow(x, 3) + coeff_y.z * pow(x, 2) + coeff_y.y * x + coeff_y.x,
        coeff_z.w * pow(x, 3) + coeff_z.z * pow(x, 2) + coeff_z.y * x + coeff_z.x
    );
}

vec3 get_vertex_lerp(int mesh_index, int smoothed) // smoothed = 1, original = 0
{
    int vert_idx = (mesh_index + smoothed * mesh_count) * vert_count * 3 + gl_VertexID * 3;
    return vec3(vert_data[vert_idx + 0], vert_data[vert_idx + 1], vert_data[vert_idx + 2]);
}
vec3 get_vertex_original(int mesh_index, bool curve_interpolation)
{
    if (curve_interpolation)
        return get_vertex_curve_fit(mesh_index, 0);
    else
        return get_vertex_lerp(mesh_index, 0);
}
vec3 get_vertex_smoothed(int mesh_index, bool curve_interpolation)
{
    if (curve_interpolation)
        return get_vertex_curve_fit(mesh_index, 1);
    else
        return get_vertex_lerp(mesh_index, 1);
}

void main(void)
{
    vec3 mask = texture(mask_texture, in_TexCoords).rgb;
   
    vec2 mouse_screen[3] = vec2[3](
        vec2(mouse_pos[0].x * 2 - 1, mouse_pos[0].y * 2 - 1),
        vec2(mouse_pos[1].x * 2 - 1, mouse_pos[1].y * 2 - 1),
        vec2(mouse_pos[2].x * 2 - 1, mouse_pos[2].y * 2 - 1)
    );

    float smooth_mix = (mask.r * mask_weight.r) + (mask.g * mask_weight.g) + (mask.b * mask_weight.b);
 
    int vert_idx = base_mesh * vert_count * 3 + gl_VertexID * 3;
    
#if 1
    vec3  d_lerp[MAX_NUMBER_OF_FACES] = vec3[MAX_NUMBER_OF_FACES]
            (vec3(0.0f), vec3(0.0f), vec3(0.0f), vec3(0.0f), vec3(0.0f), vec3(0.0f), vec3(0.0f), vec3(0.0f),
            vec3(0.0f), vec3(0.0f), vec3(0.0f), vec3(0.0f), vec3(0.0f), vec3(0.0f), vec3(0.0f), vec3(0.0f));

    vec3  d_curve[MAX_NUMBER_OF_FACES] = vec3[MAX_NUMBER_OF_FACES]
            (vec3(0.0f), vec3(0.0f), vec3(0.0f), vec3(0.0f), vec3(0.0f), vec3(0.0f), vec3(0.0f), vec3(0.0f),
            vec3(0.0f), vec3(0.0f), vec3(0.0f), vec3(0.0f), vec3(0.0f), vec3(0.0f), vec3(0.0f), vec3(0.0f));

    vec3  w[MAX_NUMBER_OF_FACES] = vec3[MAX_NUMBER_OF_FACES]
        (vec3(0.0f), vec3(0.0f), vec3(0.0f), vec3(0.0f), vec3(0.0f), vec3(0.0f), vec3(0.0f), vec3(0.0f),
        vec3(0.0f), vec3(0.0f), vec3(0.0f), vec3(0.0f), vec3(0.0f), vec3(0.0f), vec3(0.0f), vec3(0.0f));
    

    for (int i = 0; i < mesh_count; i++)
    {
        d_lerp[i] = get_vertex_lerp(i, 0) - get_vertex_lerp(i, 1);
        d_curve[i] = get_vertex_curve_fit(i, 0) - get_vertex_curve_fit(i, smooth_mix);

        vec3 dist = vec3(
            distance(mouse_screen[0], pos2d[i]),
            distance(mouse_screen[1], pos2d[i]),
            distance(mouse_screen[2], pos2d[i]));

        w[i][0] = pow(1.0f / (dist[0] + epsilon), pow_distance);
        w[i][1] = pow(1.0f / (dist[1] + epsilon), pow_distance);
        w[i][2] = pow(1.0f / (dist[2] + epsilon), pow_distance);
    }
    
    vec3 sum_w = vec3(0);
    for (int i = 0; i < mesh_count; ++i)
        sum_w += w[i];
    

    //
    // computing vertex base
    //
    vec3 vbase_lerp = vec3(0);
    {
        vec3 v_smoothed = get_vertex_smoothed(base_mesh, false);
        vbase_lerp = v_smoothed + d_lerp[base_mesh] * (1 - smooth_mix);
    }

    vec3 vbase_curve = get_vertex_curve_fit(base_mesh, smooth_mix);


    //
    // using weigthed distances
    vec3 disp_masked_lerp[3] = vec3[3](vec3(0), vec3(0), vec3(0));
    vec3 disp_masked_curve[3] = vec3[3](vec3(0), vec3(0), vec3(0));
    for (int i = 0; i < mesh_count; ++i)
    {
        disp_masked_lerp[0] = disp_masked_lerp[0] + w[i][0] * mask.r * smooth_mix * d_lerp[i];
        disp_masked_lerp[1] = disp_masked_lerp[1] + w[i][1] * mask.g * smooth_mix * d_lerp[i];
        disp_masked_lerp[2] = disp_masked_lerp[2] + w[i][2] * mask.b * smooth_mix * d_lerp[i];

        disp_masked_curve[0] = disp_masked_curve[0] + w[i][0] * mask.r * d_curve[i];
        disp_masked_curve[1] = disp_masked_curve[1] + w[i][1] * mask.g * d_curve[i];
        disp_masked_curve[2] = disp_masked_curve[2] + w[i][2] * mask.b * d_curve[i];
    }
    disp_masked_lerp[0] = disp_masked_lerp[0] / sum_w[0];
    disp_masked_lerp[1] = disp_masked_lerp[1] / sum_w[1];
    disp_masked_lerp[2] = disp_masked_lerp[2] / sum_w[2];

    disp_masked_curve[0] = disp_masked_curve[0] / sum_w[0];
    disp_masked_curve[1] = disp_masked_curve[1] / sum_w[1];
    disp_masked_curve[2] = disp_masked_curve[2] / sum_w[2];

    vec3 disp_lerp = vec3(disp_masked_lerp[0] + disp_masked_lerp[1] + disp_masked_lerp[2]);
    vec3 disp_curve = vec3(disp_masked_curve[0] + disp_masked_curve[1] + disp_masked_curve[2]);

    //
    vec4 in_Vertex_lerp  = vec4(vbase_lerp + disp_lerp * displacement_multiplier, 1);
    vec4 in_Vertex_curve = vec4(vbase_curve + disp_curve * displacement_multiplier, 1); 
#if 0  
    vec3 v_ori = vec3(vert_data[vert_idx + 0], vert_data[vert_idx + 1], vert_data[vert_idx + 2]);
    vec4 in_Vertex = vec4(v_ori, 1);
#endif    
    mat4 modelViewMatrix = viewMatrix * modelMatrix;
    mat4 normalMatrix = transpose(inverse(modelViewMatrix));
    
    //
    // computing normal
    //
    vs_out.normal = normalize(vec3(normalMatrix * vec4(in_Normal, 0.0)).xyz);
    
    //in_Vertex = in_Position;


    if (use_curve_interpolation)
    {
        vs_out.vert = modelViewMatrix * in_Vertex_curve;
        gl_Position = (projectionMatrix * modelViewMatrix) * in_Vertex_curve;
    }
    else
    {
        vs_out.vert = modelViewMatrix * in_Vertex_lerp;
        gl_Position = (projectionMatrix * modelViewMatrix) * in_Vertex_lerp;
    }


    if (invert_texture_y)
	    vs_out.texCoords = vec2(in_TexCoords.x, 1 - in_TexCoords.y);
    else
        vs_out.texCoords = in_TexCoords;

    vec4 diff_color = vec4(color_diff_multiplier, 1) * vec4(abs(in_Vertex_curve - in_Vertex_lerp));

    //int poly_idx = 1;
    //vert_idx = ((2 * mesh_count) * vert_count * 3) + (gl_VertexID * 4) + (vert_count * 4 * poly_idx);
    //vec4 poly_fit = vec4(vert_data[vert_idx + 0], vert_data[vert_idx + 1], vert_data[vert_idx + 2], vert_data[vert_idx + 3]);

    if (use_mask_color)
        vs_out.color = texture(mask_texture, in_TexCoords);
        //vs_out.color = vec4(get_rgb(), 1) * vec4(color_diff_multiplier, 1);
        //vs_out.color = diff_color;
    else
        vs_out.color = default_color;
#else
    
    //smooth_mix = mask_weight.r;

    mat4 modelViewMatrix = viewMatrix * modelMatrix;
    mat4 normalMatrix = transpose(inverse(modelViewMatrix));

    vec4 v_lerp  = mix(vec4(get_vertex_original(0, false), 1), vec4(get_vertex_smoothed(0, false), 1), 1 - smooth_mix);
    vec4 v_curve = vec4(get_vertex_curve_fit(0, 1 - smooth_mix), 1);


    vec4 diff_color = vec4(
        color_diff_multiplier[0] * abs(v_curve[0] - v_lerp[0]),
        color_diff_multiplier[1] * abs(v_curve[1] - v_lerp[1]),
        color_diff_multiplier[2] * abs(v_curve[2] - v_lerp[2]),
        1);

    

    if (use_curve_interpolation)
    {
        vs_out.vert = modelViewMatrix * v_curve;
        gl_Position = (projectionMatrix * modelViewMatrix) * v_curve;
    }
    else
    {
        vs_out.vert = modelViewMatrix * v_lerp;
        gl_Position = (projectionMatrix * modelViewMatrix) * v_lerp;
    }

    if (use_mask_color)
        //vs_out.color = texture(mask_texture, in_TexCoords);
        //vs_out.color = vec4(get_rgb(), 1);
        vs_out.color = diff_color;
    else
        vs_out.color = default_color;
#endif
}
