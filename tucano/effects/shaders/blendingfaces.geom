#version 430

layout (triangles) in;
layout (triangle_strip, max_vertices = 3) out;

in VS_OUT {
    vec4 vert;
    vec3 normal;
    vec2 texCoords;
    vec4 color;
} gs_in[];

out GS_OUT {
    vec4 vert;
    vec3 normal_vertex;
    vec3 normal_face;
    vec2 texCoords;
    vec4 color;
} gs_out;


void main()
{
    vec3 a = (gl_in[1].gl_Position - gl_in[0].gl_Position).xyz;
    vec3 b = (gl_in[2].gl_Position - gl_in[0].gl_Position).xyz;
    vec3 N = normalize(cross(b, a));
    
    
    for (int i = 0; i < gl_in.length(); ++i)
    {
        gs_out.normal_vertex = gs_in[i].normal; // using vertex normal
        gs_out.normal_face   = N; // computing face normal
        
        gl_Position = gl_in[i].gl_Position;
        gs_out.vert = gs_in[i].vert;
        gs_out.texCoords = gs_in[i].texCoords;
        gs_out.color = gs_in[i].color;
        EmitVertex();
    }
    EndPrimitive();

}  