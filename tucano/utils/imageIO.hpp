/**
 * Tucano - A library for rapid prototying with Modern OpenGL and GLSL
 * Copyright (C) 2014
 * LCG - Laboratório de Computação Gráfica (Computer Graphics Lab) - COPPE
 * UFRJ - Federal University of Rio de Janeiro
 *
 * This file is part of Tucano Library.
 *
 * Tucano Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tucano Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Tucano Library.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __IMAGEIO__
#define __IMAGEIO__

#include <tucano/config.hpp>
#include <tucano/framebuffer.hpp>

#if USE_STB_IMAGE

#include <thirdparty/stb_image/stb_image.h>
#include <thirdparty/stb_image/stb_image_write.h>


namespace Tucano
{
    namespace ImageImporter
    {
        enum class EFileFormat : uint8_t { JPG, PNG, TGA, BMP, PSD, GIF, HDR, Unknown };
        static EFileFormat isFormatSupported(const std::string& file_extension);

		static bool loadImage (const std::string& filename, Tucano::Texture* tex, bool flip_vertically = true);
		static bool loadImageToLayer (const std::string& filename, Tucano::Texture& tex, int layer);
		static bool writeImage (const std::string& filename, Tucano::Framebuffer* tex, int attach = 0, int channels = 3, bool flip_vertically = false);
   

        /* @brief Loads a texture from a supported file format.
         * Checks file extension, if format is supported reads file and loads texture
         *
         * @param filename Given filename.
         * @param tex Pointer to the texture
         * @return True if loaded successfully, false otherwise
         */
        static bool loadImage (const std::string& filename, Tucano::Texture* tex, bool flip_vertically)
        {

            int width, height, channels;
            stbi_set_flip_vertically_on_load(flip_vertically);
            float* image = stbi_loadf(filename.c_str(), &width, &height, &channels, STBI_default);

            if (image == nullptr)
            {
				std::cerr << "Error: Cannot open " << filename << std::endl; 
				return false;
            }

            GLenum format;
            switch (channels)
            {
            case STBI_rgb:        format = GL_RGB; break;
            case STBI_rgb_alpha:  format = GL_RGBA; break;
            case STBI_grey_alpha: format = GL_RG; break;
            case STBI_grey:
            default:              format = GL_RED; break;
            }

            auto success = tex->create(GL_TEXTURE_2D, GL_RGBA32F, width, height, format, GL_FLOAT, image, 0);

            stbi_image_free(image);

            #ifdef TUCANODEBUG
			Tucano::Misc::errorCheckFunc(__FILE__, __LINE__, "loading image");
            #endif

            return success > 0;

        }

        
		static bool loadImage (const std::string& filename, Tucano::Texture* tex, bool flip_vertically, float* &image, int& width, int& height, int& channels)
		{
			stbi_set_flip_vertically_on_load(flip_vertically);
			image = stbi_loadf(filename.c_str(), &width, &height, &channels, STBI_default);

			if (image == nullptr)
			{
				std::cerr << "Error: Cannot open " << filename << std::endl;
				return false;
			}

			GLenum format;
			switch (channels)
			{
			case STBI_rgb:        format = GL_RGB; break;
			case STBI_rgb_alpha:  format = GL_RGBA; break;
			case STBI_grey_alpha: format = GL_RG; break;
			case STBI_grey:
			default:              format = GL_RED; break;
			}

			auto success = tex->create(GL_TEXTURE_2D, GL_RGBA32F, width, height, format, GL_FLOAT, image, 0);

            // stbi_image_free(image);

#ifdef TUCANODEBUG
			Tucano::Misc::errorCheckFunc(__FILE__, __LINE__, "loading image");
#endif

			return success > 0;

		}

        static void freeImage(float* &image)
        {
            if (image != nullptr)
            {
                stbi_image_free(image);
                image = nullptr;
            }
        }

        static Eigen::Vector4f getPixel(float* image, int w, int h, int ch, int i, int j)
        {
            const float* p = image + (ch * (j * h + i));
            return Eigen::Vector4f
            (
                p[0], 
                p[1],
                p[2],
                //ch >= 4 ? p[3] : 0xff
                ch >= 4 ? p[3] : 1
            );
        }



		static bool loadImageToLayer (const std::string& filename, Tucano::Texture& tex, int layer)
		{
            // to be implemented
			return false;
		}

        /**
         * @brief Saves a framebuffer attachment to an image file
         *
         * @param filename Image file to save fbo attach
         * @param fbo Pointer to the Framebuffer
         * @param attach Attachment number to be saved
         */
        static bool writeImage (const std::string& filename, Tucano::Framebuffer* fbo, int attach, int channels, bool flip_vertically)
        {
			GLenum format;
			switch (channels)
			{
			case 3: format = GL_RGB; break;
			case 4: format = GL_RGBA; break;
			case 2: format = GL_RG; break;
			case 1:
			default:format = GL_RED; break;
			}


            int max_value = 255;
			Eigen::Vector2i size = fbo->getDimensions();
			GLfloat* pixels = new GLfloat[(int)(size[0] * size[1] * channels)];
			fbo->bind();
			glReadBuffer(GL_COLOR_ATTACHMENT0 + attach);
			glReadPixels(0, 0, size[0], size[1], format, GL_FLOAT, pixels);

			const std::string ext = filename.substr(filename.find_last_of(".") + 1);
            const EFileFormat file_type = isFormatSupported(ext);
            int save_result = 0;

            stbi_flip_vertically_on_write(flip_vertically);

            switch (file_type)
			{
			case EFileFormat::JPG:
				save_result = stbi_write_jpg(filename.c_str(), size[0], size[1], channels, pixels, 80);
				break;

			case EFileFormat::PNG:
				save_result = stbi_write_png(filename.c_str(), size[0], size[1], channels, pixels, 0);
				break;

			case EFileFormat::BMP:
				save_result = stbi_write_bmp(filename.c_str(), size[0], size[1], channels, pixels);
				break;

			case EFileFormat::TGA:
				save_result = stbi_write_tga(filename.c_str(), size[0], size[1], channels, pixels);
				break;

			case EFileFormat::HDR:
				save_result = stbi_write_hdr(filename.c_str(), size[0], size[1], channels, pixels);
				break;

			case EFileFormat::Unknown:
			default:
			{
				std::cerr << "format not supported : " << ext << std::endl;
                save_result = 0;
			}
			} // end switch
			
            
			fbo->unbind();
			delete[] pixels;

            if (!save_result)
			{
				std::cerr << "Error: Could not save image" << filename << std::endl;
                return false;
			}
			return true;
        }

        /* @brief
         * Checks file extension, if format is supported reads file and loads texture
         *
         * @param flie_extension Given filename extension.
         * @return True if it is supported, false otherwise
         */
        static EFileFormat isFormatSupported(const std::string& file_extension)
        {
            if (file_extension.find("jpg") != std::string::npos 
                || file_extension.find("jpeg") != std::string::npos)
                return EFileFormat::JPG;
            else if (file_extension.find("png") != std::string::npos)
            return EFileFormat::PNG;
            else if (file_extension.find("tga") != std::string::npos)
            return EFileFormat::TGA;
            else if (file_extension.find("bmp") != std::string::npos)
            return EFileFormat::BMP;
            else if (file_extension.find("psd") != std::string::npos)
            return EFileFormat::PSD;
            else if (file_extension.find("bmp") != std::string::npos)
            return EFileFormat::BMP;
            else if (file_extension.find("gif") != std::string::npos)
            return EFileFormat::GIF;
            else if (file_extension.find("hdr") != std::string::npos)
            return EFileFormat::HDR;
            else 
                return EFileFormat::Unknown;
        }
    }
}

#else

#include <tucano/utils/pamIO.hpp>
#include <tucano/utils/ppmIO.hpp>

namespace Tucano
{

namespace ImageImporter
{

#if !_MSC_VER
    static bool loadImage (string filename, Tucano::Texture* tex) __attribute__ ((unused));
    static bool loadImageToLayer (string filename, Tucano::Texture &tex, int layer) __attribute__ ((unused));
    static bool writeImage (string filename, Tucano::Framebuffer* tex, int attach = 0) __attribute__ ((unused));
    static bool searchAlternativeExtension (string filename, string ext) __attribute__ ((unused));
#else
	static bool loadImage (string filename, Tucano::Texture* tex);
	static bool loadImageToLayer (string filename, Tucano::Texture& tex, int layer);
	static bool writeImage (string filename, Tucano::Framebuffer* tex, int attach = 0);
	static bool searchAlternativeExtension (string filename, string ext);
#endif


/**
 * @brief If file format not supported searches for an alternative supported file (same name)
 * @param filename Unsupported filename without extension
 * @param ext Supported extension to search file
 */
static bool searchAlternativeExtension (string filename, string ext)
{
    string supported_file = filename + ext;
    std::ifstream infile(supported_file);
    if (infile.good())
    {        
        return true;
    }
    return false;
}


/* @brief Loads a texture from a supported file format.
 * Checks file extension, if format is supported reads file and loads texture
 *
 * @param filename Given filename.
 * @param tex Pointer to the texture
 * @return True if loaded successfully, false otherwise
 */
static bool loadImage (string filename, Tucano::Texture *tex)
{
    string ext = filename.substr(filename.find_last_of(".") + 1);

    if( ext.compare("pam") == 0)
    {
        return loadPAMImage (filename, tex);
    } 
    else if (ext.compare("ppm") == 0)
    {
        return loadPPMImage (filename, tex);
    }
    // if extesion not valid, search for same filename with same extension
    else    
    {
        std::cerr << "format not supported : " << ext.c_str() << std::endl;
        string file_without_ext = filename.substr(0, filename.find_last_of(".") + 1);
        
        if (searchAlternativeExtension (file_without_ext, "pam"))
        {
            std::cout << "using equivalent PAM file" << std::endl;
            return loadPAMImage (file_without_ext + "pam", tex);
        }
        if (searchAlternativeExtension (file_without_ext, "ppm"))
        {
            std::cout << "using equivalent PPM file" << std::endl;
            return loadPAMImage (file_without_ext + "ppm", tex);
        }
    }

    return false;
}

static bool loadImageToLayer (string filename, Tucano::Texture &tex, int layer)
{
    string ext = filename.substr(filename.find_last_of(".") + 1);

    if (ext.compare("ppm") == 0)
    {
        return loadPPMImageToLayer (filename, tex, layer);
    }

    return false;
}

/**
 * @brief Saves a framebuffer attachment to an image file
 *
 * @param filename Image file to save fbo attach
 * @param fbo Pointer to the Framebuffer
 * @param attach Attchment number to be saved
 */
static bool writeImage (string filename, Tucano::Framebuffer* fbo, int attach)
{
    string ext = filename.substr(filename.find_last_of(".") + 1);
    if( ext.compare("pam") == 0)
    {
        return writePAMImage (filename, fbo, attach);
    } 
    else if (ext.compare("ppm") == 0)
    {
        return false;//writePPMImage (filename, fbo, attach);
    }
    std::cerr << "format not supported : " << ext.c_str() << std::endl;
    return false;
}


}
}

#endif // USE_STB_IMAGE

#endif // __IMAGEIO__