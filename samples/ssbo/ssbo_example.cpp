#include "ssbo_example.hpp"
#include <tucano/config.hpp>

#include <random>

void SsboExample::initialize (int width, int height)
{
	// initialize the shader effect
	ssboEffect.initialize();

	Tucano::Misc::errorCheckFunc(__FILE__, __LINE__);
	
	camera.setPerspectiveMatrix(40.0, width / (float)height, 0.1f, 100.0f);
	camera.setViewport(Eigen::Vector2f ((float)width, (float)height));

	//Tucano::MeshImporter::loadObjFile(&mesh, TUCANO_MODEL_TOY_OBJ);
	mesh.createParallelepiped(1, 1, 1);

	mesh.normalizeModelMatrix();


	std::cout << mesh.getNumberOfVertices() << std::endl;

	////////////////////////////////////////////////////
	//
	// Generating random colors to send via ssbo
	//
	//std::random_device rd;
	//std::mt19937 gen(rd());
	//std::uniform_real_distribution<> dis(0.0, 1.0);
	const auto vert_count_3f = mesh.getNumberOfVertices() * 3;
	std::vector<float> ssbo_data = std::vector<float>(vert_count_3f * 2, 0);

	std::cout << mesh.getNumberOfVertices() << " " << vert_count_3f << std::endl;

	for (auto i = 0; i < vert_count_3f; i += 3)
	{
		ssbo_data[i + 0] = 1; //dis(gen);
		ssbo_data[i + 1] = 0; // dis(gen);
		ssbo_data[i + 2] = 0; // dis(gen);
	}
	for (auto i = vert_count_3f; i < vert_count_3f * 2; i += 3)
	{
		ssbo_data[i + 0] = 0; //dis(gen);
		ssbo_data[i + 1] = 0; // dis(gen);
		ssbo_data[i + 2] = 1; // dis(gen);
	}
	////////////////////////////////////////////////////
	//
	// Creating ssbo
	//
	ssbo = Tucano::ShaderStorageBufferFloat(ssbo_data.size());
	ssbo.initialize();
	ssbo.bind();
	glBufferData(ssbo.getBufferType(), ssbo.getSizeInBytes(), ssbo_data.data(), GL_STATIC_DRAW);
	ssbo.unbind();
	////////////////////////////////////////////////////


	glClearColor(0.2f, 0.2f, 0.3f, 1.0f);
}

void SsboExample::paintGL (void)
{
	camera.updateViewMatrix();

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	ssbo.bindBase(0);
	ssboEffect.render(mesh, camera, light);
	ssbo.unbindBase();

	camera.renderAtCorner();
}
