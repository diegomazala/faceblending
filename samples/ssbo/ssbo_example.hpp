#ifndef __NORMAL_VECTOR__
#define __NORMAL_VECTOR__

#include <GL/glew.h>

#include <tucano/effects/phongshader.hpp>
#include <tucano/effects/ssboeffect.hpp>
#include <tucano/utils/flycamera.hpp>
#include <tucano/mesh.hpp>
#include <tucano/utils/objimporter.hpp>
#include <tucano/bufferobject.hpp>

class SsboExample
{

public:

	SsboExample(void) {}

	/**
	 * @brief Initializes the shader effect
	 * @param width Window width in pixels
	 * @param height Window height in pixels
	 */
	void initialize(int width, int height);

	/**
	 * Repaints screen buffer.
	 **/
	virtual void paintGL();

	/**
	* Returns the pointer to the flycamera instance
	* @return pointer to flycamera
	**/
	Tucano::Flycamera* getCamera(void)
	{
		return &camera;
	}

private:

	// A simple phong shader for rendering meshes
	Tucano::Effects::SsboEffect ssboEffect;

	// A fly through camera
	Tucano::Flycamera camera;

	// Light represented as a camera
	Tucano::Camera light;

	// A mesh
	Tucano::Mesh mesh;

	Tucano::ShaderStorageBufferFloat ssbo = Tucano::ShaderStorageBufferFloat(0);
};

#endif // __NORMAL_VECTOR__
