#include "glfaceblending.hpp"
#include <tucano/config.hpp>
#include <tucano/thirdparty/common/vector_read_write_binary.h>
#include <tucano/thirdparty/common/timer.h>

#define USE_CUBE 0


void glFaceBlending::initialize (int width, int height)
{
    // initialize the shader effect
    phong.initialize();
	blending.initialize();

	phong.setShininessCoeff(1.0f);
	blending.setShininessCoeff(1.0f);

	camera.setPerspectiveMatrix(35.0, width/(float)height, 0.1f, 100.0f);
	camera.setViewport(Eigen::Vector2f ((float)width, (float)height));

	loadMeshes();
	
	phong.setTexture(texture);
	blending.setTexture(texture);

	Tucano::Misc::errorCheckFunc(__FILE__, __LINE__);

    glEnable(GL_DEPTH_TEST);
}

void glFaceBlending::paintGL (void)
{
   	camera.updateViewMatrix();
	
 	glClearColor(0.9, 0.9, 0.9, 0.0);
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

	switch (renderMode)
	{
	case ERenderMode::FACE_A:
	{
		meshA.modelMatrix()->setIdentity();
		meshA.modelMatrix()->translate(Eigen::Vector3f(0, 0, -1));
		phong.render(meshA, camera, light);
		break;
	}
	case ERenderMode::FACE_B:
	{
		meshB.modelMatrix()->setIdentity();
		meshB.modelMatrix()->translate(Eigen::Vector3f(0, 0, -1));
		phong.render(meshB, camera, light);
		break;
	}
	case ERenderMode::FACE_C:
	{
		meshC.modelMatrix()->setIdentity();
		meshC.modelMatrix()->translate(Eigen::Vector3f(0, 0, -1));
		blending.render(meshC, camera, light);
		break;
	}
	case ERenderMode::ALL_FACES:
	default:
	{
		meshA.modelMatrix()->setIdentity();
		meshB.modelMatrix()->setIdentity();
		meshC.modelMatrix()->setIdentity();
		meshA.modelMatrix()->translate(Eigen::Vector3f(-1, 0, -1));
		meshB.modelMatrix()->translate(Eigen::Vector3f(1, 0, -1));
		meshC.modelMatrix()->translate(Eigen::Vector3f(0, 0, -1));
		phong.render(meshA, camera, light);
		phong.render(meshB, camera, light);
		blending.render(meshC, camera, light);
		break;
	}
	}

	
	camera.renderAtCorner();

}

static float frame_counter = 0;
void glFaceBlending::update()
{
	if (isAnimEnabled())
	{
		blending.setAlphaLerp(std::sin(frame_counter += animStep) * 0.5f + 0.5);
	}
	
}


void glFaceBlending::loadMeshes()
{
#if USE_CUBE
	std::string vertfilenameA = std::string(TUCANO_MODELS_DIR) + "/dice.vert";
	std::string norfilenameA = std::string(TUCANO_MODELS_DIR) + "/dice.nor";
	
	std::string vertfilenameB = std::string(TUCANO_MODELS_DIR) + "/dice_scaled.vert";
	std::string norfilenameB = std::string(TUCANO_MODELS_DIR) + "/dice_scaled.nor";

	std::string vertfilenameAsmooth = std::string(TUCANO_MODELS_DIR) + "/dice_scaled.vert";
	std::string norfilenameAsmooth = std::string(TUCANO_MODELS_DIR) + "/dice_scaled.nor";

	std::string vertfilenameBsmooth = std::string(TUCANO_MODELS_DIR) + "/dice.vert";
	std::string norfilenameBsmooth = std::string(TUCANO_MODELS_DIR) + "/dice.nor";

	std::string uvfilename = std::string(TUCANO_MODELS_DIR) + "/dice.uv";
	std::string trifilename = std::string(TUCANO_MODELS_DIR) + "/dice.tri";
	
	// initialize texture with given image
	Tucano::ImageImporter::loadImage(std::string(TUCANO_MODELS_DIR) + "/dice.png", &texture, true);
#else
	std::string vertfilenameA = std::string(TUCANO_MODELS_DIR) + "/fig02_lap_0.vert";
	std::string norfilenameA = std::string(TUCANO_MODELS_DIR) + "/fig02_lap_0.nor";

	std::string vertfilenameB = std::string(TUCANO_MODELS_DIR) + "/fig05_lap_0.vert";
	std::string norfilenameB = std::string(TUCANO_MODELS_DIR) + "/fig05_lap_0.nor";

	std::string vertfilenameAsmooth = std::string(TUCANO_MODELS_DIR) + "/fig02_lap_20000.vert";

	std::string vertfilenameBsmooth = std::string(TUCANO_MODELS_DIR) + "/fig05_lap_20000.vert";

	std::string uvfilename = std::string(TUCANO_MODELS_DIR) + "/fig.uv";
	std::string trifilename = std::string(TUCANO_MODELS_DIR) + "/fig.tri";

	// initialize texture with given image
	Tucano::ImageImporter::loadImage(std::string(TUCANO_MODELS_DIR) + "/fig02.png", &texture, true);
#endif


	timer t_loading;

	std::vector<Eigen::Vector3f> verticesA, verticesB, verticesAsmooth, verticesBsmooth;
	std::vector<Eigen::Vector3f> normalsA, normalsB;
	std::vector<Eigen::Vector2f> texcoords;
	std::vector<uint32_t> indices;

	auto vert_count_A = vector_read(vertfilenameA, verticesA);
	auto nor_count_A = vector_read(norfilenameA, normalsA);
	auto vert_count_B = vector_read(vertfilenameB, verticesB);
	auto nor_count_B = vector_read(norfilenameB, normalsB);
	auto vert_count_A_smooth = vector_read(vertfilenameAsmooth, verticesAsmooth);
	auto vert_count_B_smooth = vector_read(vertfilenameBsmooth, verticesBsmooth);
	auto uv_count = vector_read(uvfilename, texcoords);
	auto tris_count = vector_read(trifilename, indices);

	std::cout << "Data Loaded: \n"
		<< vert_count_A << ", " << vert_count_B << " vertices\n"
		<< nor_count_A << ", " << nor_count_B << " normals\n"
		<< uv_count  << " texcoords\n"
		<< tris_count / 3 << " triangles\n";

	if (vert_count_A != vert_count_B || vert_count_A_smooth != vert_count_B_smooth || vert_count_A != vert_count_A_smooth)
	{
		std::cerr << "Error: Number of vertices does not match. Abort" << std::endl;
		return;
	}
	
	if (nor_count_A != nor_count_B)
	{
		std::cerr << "Error: Number of normals does not match. Abort" << std::endl;
		return;
	}

	std::vector<Eigen::Vector4f> vertices_4f_A(vert_count_A);
	std::vector<Eigen::Vector4f> vertices_4f_B(vert_count_B);
	

	for (auto i = 0; i < vert_count_A; ++i)
	{
		for (auto j = 0; j < 3; ++j)
		{
			vertices_4f_A[i][j] = verticesA[i][j];
			vertices_4f_B[i][j] = verticesB[i][j];
		}
		vertices_4f_A[i][3] = 1;
		vertices_4f_B[i][3] = 1;
	}

	// load attributes found in file
	meshA.loadVertices(vertices_4f_A);
	meshA.loadNormals(normalsA);
	meshA.loadTexCoords(texcoords);
	//meshA.loadColors(disp_A);
	meshA.loadIndices(indices);
	
	meshB.loadVertices(vertices_4f_B);
	meshB.loadNormals(normalsB);
	meshB.loadTexCoords(texcoords);
	//meshB.loadColors(disp_B);
	meshB.loadIndices(indices);

	meshC.loadVertices(vertices_4f_A);
	meshC.loadNormals(normalsA);
	meshC.loadTexCoords(texcoords);
	// meshC.loadColors(disp_C);
	meshC.loadIndices(indices);
	meshC.createAttribute("in_VertexB", verticesB);
	meshC.createAttribute("in_VertexADisplaced", verticesAsmooth);
	meshC.createAttribute("in_VertexBDisplaced", verticesBsmooth);

	meshC.createAttribute("in_NormalB", normalsB);

	// sets the default locations for accessing attributes in shaders
	meshA.setDefaultAttribLocations();
	meshB.setDefaultAttribLocations();
	meshC.setDefaultAttribLocations();

	meshA.normalizeModelMatrix();
	meshB.normalizeModelMatrix();
	meshC.normalizeModelMatrix();

	setRenderMode(ERenderMode::ALL_FACES);

	getPhongEffect()->enableTexture(false);
	getBlendingEffect()->enableTexture(false);

	t_loading.stop();
	t_loading.print_interval("-- Loading mesh: ");
}



