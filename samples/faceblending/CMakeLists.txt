project(faceblending)

if (MSVC)
    add_compile_options(/W4)
else()
    add_compile_options(-Wall -Wextra -Wpedantic)
endif()

add_executable(${PROJECT_NAME} main.cpp glfaceblending.cpp glfaceblending.hpp blendingeffect.hpp)
target_compile_features(${PROJECT_NAME} PRIVATE cxx_std_11)
target_link_libraries(${PROJECT_NAME} 
  tucano 
  glfw 
  common
  stb_image
  Eigen3::Eigen
  OpenGL::GL
  OpenGL::GLU
  GLEW::GLEW
)


set_property(
  TARGET ${PROJECT_NAME}
  PROPERTY COMPILE_FLAGS
  "-DTUCANOSHADERDIR=${TUCANO_EFFECTS_DIR}/shaders/"
  )


