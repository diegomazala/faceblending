#ifndef __GL_FACE_BLENDING__
#define __GL_FACE_BLENDING__

#include <GL/glew.h>

#include <tucano/effects/phongshader.hpp>
#include <tucano/utils/flycamera.hpp>
#include <tucano/utils/path.hpp>
#include <tucano/shapes/camerarep.hpp>
#include <tucano/mesh.hpp>
#include <tucano/utils/objimporter.hpp>
#include <tucano/utils/imageIO.hpp>
#include "blendingeffect.hpp"

class glFaceBlending 
{

public:

	enum class ERenderMode : uint8_t
	{
		ALL_FACES,
		FACE_A,
		FACE_B,
		FACE_C
	};

    glFaceBlending(void){}
    
    /**
     * @brief Initializes the shader effect
	 * @param width Window width in pixels
	 * @param height Window height in pixels
     */
    void initialize(int width, int height);

    /**
     * Repaints screen buffer.
     **/
    virtual void paintGL();

	/**
	 * Update scene logic
	 **/
	virtual void update();

	/**
	* Returns the pointer to the flycamera instance
	* @return pointer to flycamera
	**/
    Tucano::Flycamera* getCamera(void)
	{
		return &camera;
	}

	Tucano::Mesh* getMesh()
	{
		return &meshA;
	}

	Tucano::Effects::Phong* getPhongEffect() { return &phong; }
	Tucano::Effects::Blending* getBlendingEffect() { return &blending; }

	void enableAnim(bool enable) { animationEnabled = enable; }
	bool isAnimEnabled() const { return animationEnabled; }

	void setRenderMode(ERenderMode value) { renderMode = value; }
	ERenderMode getRenderMode() const { return renderMode;}

private:

	void loadMeshes();

	// A simple phong shader for rendering meshes
    Tucano::Effects::Phong phong;
	Tucano::Effects::Blending blending;

	// A fly through camera
    Tucano::Flycamera camera;

	// Light represented as a camera
	Tucano::Camera light;

	/// Texture to hold input image
	Tucano::Texture texture;

	// A mesh
    Tucano::Mesh meshA;
	Tucano::Mesh meshB;
	Tucano::Mesh meshC;

	bool animationEnabled;
	float animStep = 0.01f;

	ERenderMode renderMode = ERenderMode::ALL_FACES;
};

#endif // __GL_FACE_BLENDING__
