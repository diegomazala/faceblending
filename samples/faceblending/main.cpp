#include <iostream>
#include "glfaceblending.hpp"
#include "GLFW/glfw3.h"

#define WINDOW_WIDTH 1920
#define WINDOW_HEIGHT 1080

glFaceBlending *glScene;

void initialize (void)
{
    Tucano::Misc::initGlew();
	glScene = new glFaceBlending();
	glScene->initialize(WINDOW_WIDTH, WINDOW_HEIGHT);
	cout << "initialized" << endl;

    cout << endl << endl << " ************ usage ************** " << endl;
    cout << " R : reset camera" << endl;
    cout << "ASWD : move Left, Rigth, Forward, Back" << endl;
    cout << "EC : move Up, Down" << endl;
    cout << "K : add key point" << endl;
    cout << "SPACE : start/stop camera animation" << endl;
    cout << ",. : step forward/backward in animation" << endl;
    cout << "0 : toggle draw control points" << endl;
    cout << "Z-X : shininess" << endl;
	cout << "T : texture on/off" << endl;
    cout << " ***************** " << endl;
}

void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, 1);	
	if (key == GLFW_KEY_R && action == GLFW_PRESS)
		glScene->getCamera()->reset();
	if (key == GLFW_KEY_A && (action == GLFW_PRESS || action == GLFW_REPEAT))
		glScene->getCamera()->strideLeft();
	if (key == GLFW_KEY_D && (action == GLFW_PRESS || action == GLFW_REPEAT))
		glScene->getCamera()->strideRight();
	if (key == GLFW_KEY_S && (action == GLFW_PRESS || action == GLFW_REPEAT))
		glScene->getCamera()->moveBack();
	if (key == GLFW_KEY_W && (action == GLFW_PRESS || action == GLFW_REPEAT))
		glScene->getCamera()->moveForward();
 	if (key == GLFW_KEY_C && (action == GLFW_PRESS || action == GLFW_REPEAT))
		glScene->getCamera()->moveDown();
	if (key == GLFW_KEY_E && (action == GLFW_PRESS || action == GLFW_REPEAT))
		glScene->getCamera()->moveUp();	

	if (key == GLFW_KEY_DELETE && (action == GLFW_PRESS || action == GLFW_REPEAT))
		glScene->getMesh()->modelMatrix()->translate(Eigen::Vector3f(-1, 0, 0));
	if (key == GLFW_KEY_PAGE_DOWN && (action == GLFW_PRESS || action == GLFW_REPEAT))
		glScene->getMesh()->modelMatrix()->translate(Eigen::Vector3f(1, 0, 0));
	if (key == GLFW_KEY_HOME && (action == GLFW_PRESS || action == GLFW_REPEAT))
		glScene->getMesh()->modelMatrix()->translate(Eigen::Vector3f(0, 0, -1));
	if (key == GLFW_KEY_END && (action == GLFW_PRESS || action == GLFW_REPEAT))
		glScene->getMesh()->modelMatrix()->translate(Eigen::Vector3f(0, 0, 1));

	if (key == GLFW_KEY_Z && (action == GLFW_PRESS || action == GLFW_REPEAT))
	{
		glScene->getPhongEffect()->setShininessCoeff(glScene->getPhongEffect()->getShininessCoeff() - 0.1);
		glScene->getBlendingEffect()->setShininessCoeff(glScene->getBlendingEffect()->getShininessCoeff() - 0.1);
	}
	if (key == GLFW_KEY_X && (action == GLFW_PRESS || action == GLFW_REPEAT))
	{
		glScene->getPhongEffect()->setShininessCoeff(glScene->getPhongEffect()->getShininessCoeff() + 0.1);
		glScene->getBlendingEffect()->setShininessCoeff(glScene->getBlendingEffect()->getShininessCoeff() + 0.1);
	}

	if (key == GLFW_KEY_T && (action == GLFW_PRESS || action == GLFW_REPEAT))
	{
		glScene->getPhongEffect()->enableTexture(!glScene->getPhongEffect()->isTextureEnabled());
		glScene->getBlendingEffect()->enableTexture(!glScene->getBlendingEffect()->isTextureEnabled());
	}

	for (auto num_key = GLFW_KEY_0; num_key <= GLFW_KEY_9; ++num_key)
	{
		if (key == num_key && (action == GLFW_PRESS || action == GLFW_REPEAT))
		{
			glScene->getBlendingEffect()->setAlphaLerp(float((num_key - GLFW_KEY_0) * 0.11f));
			glScene->enableAnim(false);
		}
	}

	if (key == GLFW_KEY_SPACE && (action == GLFW_PRESS || action == GLFW_REPEAT))
	{
		glScene->enableAnim(!glScene->isAnimEnabled());
	}

	if (key == GLFW_KEY_TAB && (action == GLFW_PRESS || action == GLFW_REPEAT))
	{
		glScene->getBlendingEffect()->setBaseMesh(!glScene->getBlendingEffect()->getBaseMesh());
	}

	if (key == GLFW_KEY_N && (action == GLFW_PRESS || action == GLFW_REPEAT))
	{
		glScene->getBlendingEffect()->enableFaceNormal(!glScene->getBlendingEffect()->isFaceNormalEnabled());
	}

	for (auto num_key = GLFW_KEY_F1; num_key <= GLFW_KEY_F4; ++num_key)
	{
		if (key == num_key && (action == GLFW_PRESS || action == GLFW_REPEAT))
		{
			glScene->setRenderMode(static_cast<glFaceBlending::ERenderMode>(num_key - GLFW_KEY_F1));
		}
	}
}

static void mouseButtonCallback (GLFWwindow* window, int button, int action, int mods)
{
    if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS)
	{
		double xpos, ypos;
		glfwGetCursorPos(window, &xpos, &ypos);
		glScene->getCamera()->startRotation( Eigen::Vector2f (xpos, ypos) );
	}
}
static void cursorPosCallback(GLFWwindow* window, double xpos, double ypos)
{
	if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_1) == GLFW_PRESS)
	{
		glScene->getCamera()->rotate(Eigen::Vector2f(xpos, ypos));
	}
}


int main(int argc, char *argv[])
{
	GLFWwindow* main_window;

	if (!glfwInit()) 
	{
    	std::cerr << "Failed to init glfw" << std::endl;
		return 1;
	}

	// double x dimension for splitview and add margin
	main_window = glfwCreateWindow(WINDOW_WIDTH, WINDOW_HEIGHT, "TUCANO :: Face Blending", NULL, NULL);
	if (!main_window)
	{
		std::cerr << "Failed to create the GLFW window" << std::endl;
		glfwTerminate();
		return -1;
	}

	glfwMakeContextCurrent(main_window);
	glfwSetKeyCallback(main_window, keyCallback);
	glfwSetMouseButtonCallback(main_window, mouseButtonCallback);
	glfwSetCursorPosCallback(main_window, cursorPosCallback);

	glfwSetInputMode(main_window, GLFW_STICKY_KEYS, true);

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
   	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
   	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	initialize();


	while (!glfwWindowShouldClose(main_window))
	{
		glfwMakeContextCurrent(main_window);
		glScene->update();
		glScene->paintGL();
		glfwSwapBuffers( main_window );

		glfwPollEvents();
	}

	glfwDestroyWindow(main_window);
	glfwTerminate();
	return 0;
}
