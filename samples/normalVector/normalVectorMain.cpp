#include <iostream>
#include "normalVector.hpp"
#include "GLFW/glfw3.h"

#define WINDOW_WIDTH 1280
#define WINDOW_HEIGHT 720

NormalVector *scene;

void initialize (void)
{
    Tucano::Misc::initGlew();
	scene = new NormalVector();
	scene->initialize(WINDOW_WIDTH, WINDOW_HEIGHT);
	cout << "initialized" << endl;

    cout << endl << endl << " ************ usage ************** " << endl;
    cout << " R : reset camera" << endl;
    cout << "ASWD : move Left, Rigth, Forward, Back" << endl;
    cout << "EC : move Up, Down" << endl;
    cout << "K : add key point" << endl;
    cout << "SPACE : start/stop camera animation" << endl;
    cout << ",. : step forward/backward in animation" << endl;
    cout << "0 : toggle draw control points" << endl;
    //cout << "9 : toggle draw quaternions" << endl;
    cout << " ***************** " << endl;
}

void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, 1);	
	if (key == GLFW_KEY_R && action == GLFW_PRESS)
		scene->getCamera()->reset();
	if (key == GLFW_KEY_A && (action == GLFW_PRESS || action == GLFW_REPEAT))
		scene->getCamera()->strideLeft();
	if (key == GLFW_KEY_D && (action == GLFW_PRESS || action == GLFW_REPEAT))
		scene->getCamera()->strideRight();
	if (key == GLFW_KEY_S && (action == GLFW_PRESS || action == GLFW_REPEAT))
		scene->getCamera()->moveBack();
	if (key == GLFW_KEY_W && (action == GLFW_PRESS || action == GLFW_REPEAT))
		scene->getCamera()->moveForward();
 	if (key == GLFW_KEY_C && (action == GLFW_PRESS || action == GLFW_REPEAT))
		scene->getCamera()->moveDown();
	if (key == GLFW_KEY_E && (action == GLFW_PRESS || action == GLFW_REPEAT))
		scene->getCamera()->moveUp();	
	if (key == GLFW_KEY_UP && (action == GLFW_PRESS || action == GLFW_REPEAT))
		scene->increaseNormalVectorMagnitude();
	if (key == GLFW_KEY_DOWN && (action == GLFW_PRESS || action == GLFW_REPEAT))
		scene->decreaseNormalVectorMagnitude();
}

static void mouseButtonCallback (GLFWwindow* window, int button, int action, int mods)
{
    if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS)
	{
		double xpos, ypos;
		glfwGetCursorPos(window, &xpos, &ypos);
		scene->getCamera()->startRotation( Eigen::Vector2f (xpos, ypos) );
	}
}
static void cursorPosCallback(GLFWwindow* window, double xpos, double ypos)
{
	if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_1) == GLFW_PRESS)
	{
		scene->getCamera()->rotate(Eigen::Vector2f(xpos, ypos));
	}
}


int main(int argc, char *argv[])
{
	GLFWwindow* main_window;

	if (!glfwInit()) 
	{
    	std::cerr << "Failed to init glfw" << std::endl;
		return 1;
	}

	// double x dimension for splitview and add margin
	main_window = glfwCreateWindow(WINDOW_WIDTH, WINDOW_HEIGHT, "TUCANO :: Normal Vector", NULL, NULL);
	if (!main_window)
	{
		std::cerr << "Failed to create the GLFW window" << std::endl;
		glfwTerminate();
		return -1;
	}

	glfwMakeContextCurrent(main_window);
	glfwSetKeyCallback(main_window, keyCallback);
	glfwSetMouseButtonCallback(main_window, mouseButtonCallback);
	glfwSetCursorPosCallback(main_window, cursorPosCallback);

	glfwSetInputMode(main_window, GLFW_STICKY_KEYS, true);

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
   	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
   	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	initialize();


	while (!glfwWindowShouldClose(main_window))
	{
		glfwMakeContextCurrent(main_window);
		scene->paintGL();
		glfwSwapBuffers( main_window );

		glfwPollEvents();
	}

	glfwDestroyWindow(main_window);
	glfwTerminate();
	return 0;
}
