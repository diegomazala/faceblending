#ifndef __NORMAL_VECTOR__
#define __NORMAL_VECTOR__

#include <GL/glew.h>

#include <tucano/effects/phongshader.hpp>
#include <tucano/effects/normalvector.hpp>
#include <tucano/utils/flycamera.hpp>
#include <tucano/mesh.hpp>
#include <tucano/utils/objimporter.hpp>

class NormalVector 
{

public:

    NormalVector(void){}
    
    /**
     * @brief Initializes the shader effect
	 * @param width Window width in pixels
	 * @param height Window height in pixels
     */
    void initialize(int width, int height);

    /**
     * Repaints screen buffer.
     **/
    virtual void paintGL();

	/**
	* Returns the pointer to the flycamera instance
	* @return pointer to flycamera
	**/
    Tucano::Flycamera* getCamera(void)
	{
		return &camera;
	}

	void increaseNormalVectorMagnitude() 
	{
		normalVectorEffect.setMagnitude(normalVectorEffect.getMagnitude() * 1.25f);
	}

	void decreaseNormalVectorMagnitude()
	{
		normalVectorEffect.setMagnitude(normalVectorEffect.getMagnitude() * 0.75f);
	}

private:

	// A simple phong shader for rendering meshes
    Tucano::Effects::Phong phong;
	Tucano::Effects::NormalVector normalVectorEffect;

	// A fly through camera
    Tucano::Flycamera camera;

	// Light represented as a camera
	Tucano::Camera light;

	// A mesh
    Tucano::Mesh mesh;
};

#endif // __NORMAL_VECTOR__
