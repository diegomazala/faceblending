#include "normalVector.hpp"
#include <tucano/config.hpp>

void NormalVector::initialize (int width, int height)
{
    // initialize the shader effect
    phong.initialize();
	normalVectorEffect.initialize();

	Tucano::Misc::errorCheckFunc(__FILE__, __LINE__);

	camera.setPerspectiveMatrix(40.0, width/(float)height, 0.1f, 100.0f);
	camera.setViewport(Eigen::Vector2f ((float)width, (float)height));

    Tucano::MeshImporter::loadObjFile(&mesh, TUCANO_MODEL_TOY_OBJ);
	
	mesh.normalizeModelMatrix();

	glClearColor(0.2f, 0.2f, 0.3f, 1.0f);
}

void NormalVector::paintGL (void)
{
	camera.updateViewMatrix();

    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

	phong.render(mesh, camera, light);
	normalVectorEffect.render(mesh, camera);
	camera.renderAtCorner();
}
