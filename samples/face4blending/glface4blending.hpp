#ifndef __GL_FACE_BLENDING__
#define __GL_FACE_BLENDING__

#include <GL/glew.h>

#include <tucano/effects/phongshader.hpp>
#include <tucano/utils/flycamera.hpp>
#include <tucano/utils/path.hpp>
#include <tucano/shapes/camerarep.hpp>
#include <tucano/mesh.hpp>
#include <tucano/utils/objimporter.hpp>
#include <tucano/utils/imageIO.hpp>
#include <tucano/effects/rendertexture.hpp>
#include <tucano/gui/base.hpp>
#include <tucano/gui/button.hpp>
#include <tucano/gui/slider.hpp>
#include <tucano/bufferobject.hpp>
#include "blending4effect.hpp"
#include "blendingproject.hpp"
#include "pbo.hpp"

class glFace4Blending
{

public:

	enum class ERenderMode : uint8_t
	{
		ALL_FACES,
		FACE_A,
		FACE_B,
		FACE_C
	};

	glFace4Blending(void) {}
	~glFace4Blending(){Tucano::ImageImporter::freeImage(maskTexData);}
    
    /**
     * @brief Initializes the shader effect
	 * @param width Window width in pixels
	 * @param height Window height in pixels
     */
    void initialize(int width, int height, const char* projectfilepath);

    /**
     * Repaints screen buffer.
     **/
    virtual void paintGL();

	/**
	 * Update scene logic
	 **/
	virtual void update();

	/**
	* Returns the pointer to the flycamera instance
	* @return pointer to flycamera
	**/
    Tucano::Flycamera* getCamera(void)
	{
		return &camera;
	}

	Tucano::Mesh* getMesh()
	{
		return &mesh;
	}

	Tucano::GUI::Base* getGUI (void)
	{
		return &gui;
	}

	Tucano::Effects::Blending4* getBlendingEffect() { return &blending; }

	void setNormalVertFaceLerp(float value) { normalVertFaceLerpSlider.moveSlider(value); }

	void enableAnim(bool enable) { animationEnabled = enable; }
	bool isAnimEnabled() const { return animationEnabled; }

	void setRenderMode(ERenderMode value) { renderMode = value; }
	ERenderMode getRenderMode() const { return renderMode;}

	void onMouseMove(double xpos, double ypos);
	void keyModifierPressed(bool left_ctrl, bool left_shift, bool left_alt);

	bool saveMesh(const std::string & filename, bool invert_face = true);

	void enableRenderGUI(bool enabled) { renderGUIEnabled = enabled; }
	bool isRenderGUIEnabled() const { return renderGUIEnabled; }

	void saveScreenshot(const char* filename);
	void onSaveButtonPressed();
	void onScreenshotButtonPressed();
	void setMousePos(int x, int y) { mousePos = { x, y }; }



	Eigen::Vector3f get_vertex_from_mesh_original(std::size_t mesh_index, std::size_t vert_index) const;
	Eigen::Vector3f get_vertex_from_mesh_smoothed(std::size_t mesh_index, std::size_t vert_index) const;
	Eigen::Vector3f get_vertex_curve_fit(std::size_t mesh_index, std::size_t vert_index, float mix) const;

private:

	
	void onButtonPressed(int button_index);
	void onButtonHovering(int button_index);
	void onButtonReleased(int button_index);
	void onTextureCheckboxClicked(int button_index);
	void onTargetButtonPressed(int button_index);
	void initializeGUI(int width, int height);
	

	void loadMeshes();

	// A simple phong shader for rendering meshes
	Tucano::Effects::Blending4 blending;

	// A fly through camera
    Tucano::Flycamera camera;

	// Light represented as a camera
	Tucano::Camera light;

	/// Texture to hold input image
	std::vector<Tucano::Texture> texture;

	Tucano::Texture maskTexture;
	float* maskTexData = nullptr;
	int maskTexWidth = 0;
	int maskTexHeight = 0;
	int maskTexChannels = 0;

	// A mesh
	Tucano::Mesh mesh;

	bool animationEnabled = false;
	float animStep = 0.005f;

	ERenderMode renderMode = ERenderMode::ALL_FACES;

	BlendingProject project;
	bool renderGUIEnabled = true;

	//
	// UI Attributes
	//
	Tucano::GUI::Base gui;
	std::vector<Tucano::GUI::Slider> maskSlider = std::vector<Tucano::GUI::Slider>(3);
	std::vector<Tucano::GUI::Button> thumbnail = std::vector<Tucano::GUI::Button>(4);
	Tucano::GUI::Slider displacementSlider;
	Tucano::GUI::Slider normalVertFaceLerpSlider;
	Tucano::GUI::Slider colorDiffSlider[3];
	Tucano::GUI::Button saveButton;
	Tucano::GUI::Button screenshotButton;
	Tucano::GUI::Button selectionButton;
	std::vector<Tucano::GUI::Button> targetButton = std::vector<Tucano::GUI::Button>(3);
	Tucano::GUI::Button* currentTargetButton = nullptr;
	std::vector<Tucano::GUI::SelectButton> maskCheckbox = std::vector<Tucano::GUI::SelectButton>(3);
	std::vector<Tucano::GUI::SelectButton> textureCheckbox = std::vector<Tucano::GUI::SelectButton>(4);
	bool leftCtrl = false;
	bool leftShift = false;
	bool leftAlt = false;
	Eigen::Vector2i mousePos;
	Eigen::Vector2f viewportSize;


	std::vector<std::vector<float>> vertOriginal;
	std::vector<std::vector<float>> vertSmoothed;
	std::vector<std::vector<float>> polyfit;

	std::vector<Eigen::Vector3f> normals;
	std::vector<Eigen::Vector2f> texcoords;
	//std::vector<Eigen::Vector3f> rgb;
	std::vector<uint32_t> indices;

	gl::pbo pbo;

	Tucano::ShaderStorageBufferFloat ssbo = Tucano::ShaderStorageBufferFloat(0);
};

#endif // __GL_FACE_BLENDING__
