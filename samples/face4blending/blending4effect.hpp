/**
 * Tucano - A library for rapid prototying with Modern OpenGL and GLSL
 * Copyright (C) 2014
 * LCG - Laboratório de Computação Gráfica (Computer Graphics Lab) - COPPE
 * UFRJ - Federal University of Rio de Janeiro
 *
 * This file is part of Tucano Library.
 *
 * Tucano Library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tucano Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Tucano Library.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __FACE_EFFECT__
#define __FACE_EFFECT__

#include "tucano/effect.hpp"
#include "tucano/camera.hpp"
#include "tucano/mesh.hpp"
#include "tucano/texture.hpp"

namespace Tucano
{
namespace Effects
{

/**
 * @brief Renders a mesh using a Phong shader.
 */
class Blending4 : public Tucano::Effect
{

private:

	/// Phong Shader
	Tucano::Shader shader;

	/// Default color
	Eigen::Vector4f default_color = Eigen::Vector4f (0.75, 0.75, 0.75, 1.0);

	/// Ambient coefficient
	Eigen::Vector3f ka = Eigen::Vector3f(0.1, 0.1, 0.1);

	/// Diffuse coefficient
	Eigen::Vector3f kd = Eigen::Vector3f(0.75, 0.75, 0.75);

	/// Specular coefficient
	Eigen::Vector3f ks = Eigen::Vector3f(0.4, 0.4, 0.4);

	/// Shininess    
	float shininess = 10;

	/// Texture
	Tucano::Texture texture;
    Tucano::Texture maskTexture;
    bool use_mask_color = false;
    bool use_curve_interpolation = true;

	/// Weight for displacement vector
    float displacementMultiplier = 1.0f;

    /// Weight for color difference error
    Eigen::Vector3f colorDiffMultiplier = Eigen::Vector3f(1.0f, 1.0f, 1.0f);

	/// base mesh (0 or 1)
	int baseMesh = 0;

    /// True of false when using texture
    bool textureEnabled = true;
    bool invertTextureY = false;

    /// Lerp between face normal computation in geometry shader
    float normalVertFaceLerp = 1.0f;

    Eigen::Vector3f maskWeight = Eigen::Vector3f(1.0f, 1.0f, 1.0f);

    // number of meshes being processed
    int mesh_count = 4;

public:

    std::vector<Eigen::Vector2f> mouse_pos = {Eigen::Vector2f(.5f, .5f), Eigen::Vector2f(.5f, .5f), Eigen::Vector2f(.5f, .5f)};

    std::vector<Eigen::Vector2f> pos2d = 
    {
        Eigen::Vector2f(0, 1),
        Eigen::Vector2f(0, -1),
        Eigen::Vector2f(-1, 0),
        Eigen::Vector2f(1, 0),
        Eigen::Vector2f(-0.75, 0.75),
		Eigen::Vector2f(-0.75, -0.75),
		Eigen::Vector2f(0.75, 0.75),
		Eigen::Vector2f(0.75, -0.75),
		Eigen::Vector2f(-0.375, 0.875),
		Eigen::Vector2f(-0.375, -0.875),
		Eigen::Vector2f(0.375, 0.875),
		Eigen::Vector2f(0.375, -0.875),
		Eigen::Vector2f(-0.875, 0.375),
		Eigen::Vector2f(-0.875, -0.375),
		Eigen::Vector2f(0.875, 0.375),
		Eigen::Vector2f(0.875, -0.375),
    };

   

    /**
     * @brief Default constructor.
     */
    Blending4 (void)
    {}

    /**
     * @brief Load and initialize shaders
     */
    virtual void initialize (void)
    {
        // searches in default shader directory (/shaders) for shader files phongShader.(vert,frag,geom,comp)
        loadShader(shader, "blendingfaces");
    }

    /**
    * @brief Sets the default color, usually used for meshes without color attribute
    */
    void setDefaultColor (const Eigen::Vector4f& color)
    {
        default_color = color;
    }

    /**
    * @brief Set ambient coefficient
    * @param value New ambient coeff (ka)
    */
    void setAmbientCoeff (Eigen::Vector3f value)
    {
        ka = value;
    }

    /**
    * @brief Set diffuse coefficient
    * @param value New diffuse coeff (kd)
    */
    void setDiffuseCoeff (Eigen::Vector3f value)
    {
        kd = value;
    }

    /**
    * @brief Set specular coefficient
    * @param New specular coeff (ks)
    */
    void setSpecularCoeff (Eigen::Vector3f value)
    {
        ks = value;
    }

    /**
    * @brief Set shininess exponent
    * @param New shininess coeff (shininess)
    */
    void setShininessCoeff (float value)
    {
        shininess = value;
    }
    float getShininessCoeff () const { return shininess; }

    /**
     * @brief Set a texture for the model
     * Note that the mesh must contain tex coords to work properly with texture
     * @param tex Given texture
     */
    void setTexture (const Tucano::Texture& tex)
    {
        texture = tex;
    }

	void setMaskTexture (const Tucano::Texture& tex)
	{
		maskTexture = tex;
	}

    void setMeshCount(int count) { mesh_count = count; }
    int getMeshCount() const { return mesh_count; }

	void useMaskColor(bool enable) { use_mask_color = enable; }
	bool useMaskColor() const { return use_mask_color; }

	void useCurveInterpolation(bool enable) { use_curve_interpolation = enable; }
	bool useCurveInterpolation() const { return use_curve_interpolation; }

    void enableTexture(bool enable) { textureEnabled = enable;  }
    bool isTextureEnabled() const { return textureEnabled; }

	void invertTextureVertically(bool enable) { invertTextureY = enable; }
	bool invertTextureVertically() const { return invertTextureY; }

    Eigen::Vector3f getDiffuseCoeff (void) { return kd; }
    Eigen::Vector3f getAmbientCoeff (void) { return ka; }
    Eigen::Vector3f getSpecularCoeff (void) { return ks; }
    float getShininessCoeff (void) { return shininess; }

    Tucano::Texture* getTexture (void) { return &texture; }
    Tucano::Texture* getMaskTexture (void) { return &maskTexture; }
	void setMaskWeight(int index, float value) { maskWeight[index] = value; }
	float getMaskWeight(int index) { return maskWeight[index]; }

	void setDisplacementMultiplier(float value) { displacementMultiplier = value; }
	float getDisplacementMultiplier() { return displacementMultiplier; }

	void setColorDiffMultiplier(float value, int index) { colorDiffMultiplier[index] = value; }
	float getColorDiffMultiplier(int index) { return colorDiffMultiplier[index]; }

	void setNormalVertFaceLerp(float value) { normalVertFaceLerp = value; }
	float getNormalVertFaceLerp() { return normalVertFaceLerp; }
    

    void setBaseMesh (int value) { baseMesh = value; }
    int getBaseMesh () const { return baseMesh; }

    /** 
     * @brief Render the mesh given a camera and light, using a Phong shader 
     * @param mesh Given mesh
     * @param camera Given camera 
     * @param lightTrackball Given light camera 
     */
    void render (Tucano::Mesh& mesh, const Tucano::Camera& camera, const Tucano::Camera& lightTrackball)
    {

        Eigen::Vector4f viewport = camera.getViewport();
        glViewport(viewport[0], viewport[1], viewport[2], viewport[3]);

        shader.bind();

        // sets all uniform variables for the phong shader
        shader.setUniform("projectionMatrix", camera.getProjectionMatrix());
        shader.setUniform("modelMatrix", mesh.getShapeModelMatrix());
        shader.setUniform("viewMatrix", camera.getViewMatrix());
        shader.setUniform("lightViewMatrix", lightTrackball.getViewMatrix());
        shader.setUniform("has_color", mesh.hasAttribute("in_Color"));
        shader.setUniform("default_color", mesh.getColor());
        shader.setUniform("ka", ka);
        shader.setUniform("kd", kd);
        shader.setUniform("ks", ks);
        shader.setUniform("shininess", shininess);
        shader.setUniform("base_mesh", baseMesh);
        shader.setUniform("normal_vert_face_lerp", normalVertFaceLerp);

        shader.setUniform("displacement_multiplier", displacementMultiplier);

        shader.setUniform("color_diff_multiplier", colorDiffMultiplier);

        shader.setUniform("mesh_count", mesh_count);
        shader.setUniform("vert_count", mesh.getNumberOfVertices());

        shader.setUniform("mouse_pos", mouse_pos.data()->data(), 2, (GLsizei)mouse_pos.size());

        shader.setUniform("pos2d", pos2d.data()->data(), 2, (GLsizei)pos2d.size());

        //shader.setUniform("pos_0", anchorPos[0]);
        //shader.setUniform("pos_1", anchorPos[1]);
        //shader.setUniform("pos_2", anchorPos[2]);
        //shader.setUniform("pos_3", anchorPos[3]);
        //shader.setUniform("pos_4", anchorPos[4]);
        //shader.setUniform("pos_5", anchorPos[5]);
        //shader.setUniform("pos_6", anchorPos[6]);
        //shader.setUniform("pos_7", anchorPos[7]);
        shader.setUniform("use_curve_interpolation", use_curve_interpolation);
        shader.setUniform("use_mask_color", use_mask_color);
        shader.setUniform("mask_weight", maskWeight);
        shader.setUniform("mask_texture", maskTexture.bind());
        shader.setUniform("invert_texture_y", invertTextureY);
        

        bool has_texture = mesh.hasAttribute("in_TexCoords") && !texture.isEmpty() && isTextureEnabled();
 
        shader.setUniform("has_texture", has_texture);
        if (has_texture)
            shader.setUniform("model_texture", texture.bind());
        else
            shader.setUniform("model_texture", 0);

        mesh.setAttributeLocation(shader);

        mesh.render();

        shader.unbind();
        if (has_texture)
            texture.unbind();

        maskTexture.unbind();
    }


};
}
}


#endif // __FACE_EFFECT__
