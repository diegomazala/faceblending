#include "glface4blending.hpp"
#include <tucano/config.hpp>
#include <tucano/thirdparty/common/vector_read_write_binary.h>
#include <tucano/thirdparty/common/timer.h>
#include "tinyfiledialogs.h"
#include "tinyobj.h"
#include <Windows.h>

const int thumb_size = 128;
const int button_size = 32;
const int button_half_size = button_size / 2;

void glFace4Blending::onSaveButtonPressed()
{
	char const* filterPatterns[1] = { "*.obj" };
	const char* saveFileName = tinyfd_saveFileDialog(
		"Save Blending Parameters",
		"untitled.obj",
		1,
		filterPatterns,
		NULL);

	if (saveFileName)
	{
		HCURSOR wait_cursor = LoadCursor(NULL, IDC_WAIT);     // wait cursor
		HCURSOR prev_cursor = GetCursor();
		SetCursor(wait_cursor);

		if (saveMesh(saveFileName))
		{
			std::string msg = "File saved successfully at ";
			msg += saveFileName;
			tinyfd_messageBox("Save obj file", msg.c_str(),	"ok", "info", 1);
			system(std::string("explorer " + std::string(saveFileName)).c_str());
		}
		else
		{
			std::string msg = "Could not save obj file: ";
			msg += saveFileName;
			tinyfd_messageBox("Error", msg.c_str(), "ok", "error", 1);
		}
		
		SetCursor(prev_cursor);
	}
}

void glFace4Blending::onScreenshotButtonPressed()
{
	std::thread t([&]
	{
		//auto renderGUI = renderGUIEnabled;
		//renderGUIEnabled = false;

		char const* filterPatterns[1] = { "*.png" };
		const char* saveFileName = tinyfd_saveFileDialog(
			"Save Screenshot",
			"screenshot.png",
			1,
			filterPatterns,
			NULL);

		if (saveFileName)
		{
			saveScreenshot(saveFileName);
		}

		//renderGUIEnabled = renderGUI;
	});
	t.detach();
}

void glFace4Blending::onTextureCheckboxClicked(int tex_button_index)
{
	textureCheckbox[tex_button_index].isSelected()
	? textureCheckbox[tex_button_index].setColor({ 1.f, 1.f, 1.f, 1.f })
	: textureCheckbox[tex_button_index].setColor({ 0.7f, 0.7f, 0.7f, 1.f });

	blending.enableTexture(textureCheckbox[tex_button_index].isSelected());
	blending.setTexture(texture[tex_button_index]);

	for (auto i = 0; i < textureCheckbox.size(); ++i)
	{
		if (i != tex_button_index)
		{
			textureCheckbox[i].setSelected(false);
			textureCheckbox[i].setColor({ 0.7f, 0.7f, 0.7f, 1.f });
		}
	}
}



void glFace4Blending::onTargetButtonPressed(int button_index)
{
	currentTargetButton = &targetButton[button_index];
}

void glFace4Blending::onButtonPressed(int button_index)
{
	if (leftCtrl)
	{
		blending.setBaseMesh(button_index);
		auto x = mousePos.x() - button_size * 2;
		auto y = mousePos.y() - button_size * 3;
		selectionButton.setPosition(x, y);
	}
}


void glFace4Blending::keyModifierPressed(bool left_ctrl, bool left_shift, bool left_alt)
{
	leftCtrl = left_ctrl;
	leftShift = left_shift;
	leftAlt = left_alt;
}


void glFace4Blending::onMouseMove(double xpos, double ypos)
{
	int button_pressed = -1;
	for (auto i = 0; i < thumbnail.size(); ++i)
	{
		if (thumbnail[i].isPressed())
			button_pressed = i;
	}
	if (button_pressed > -1)
	{
		auto x = xpos - thumb_size * 0.5;
		auto y = ypos - thumb_size * 0.5;
		thumbnail[button_pressed].setPosition(x, y);
		if (blending.getBaseMesh() == button_pressed)
			selectionButton.setPosition(x, y - (thumb_size * 0.25));
	}

	if (leftAlt)
	{
		double x = (xpos / viewportSize.x());
		double y = 1 - (ypos / viewportSize.y());

		for (auto i = 0; i < targetButton.size(); ++i)
		{
			if (currentTargetButton == &targetButton[i])
			{
				blending.mouse_pos[i] = { x, y };

				int xx = x * gui.getViewportSize().x() - button_half_size;
				int yy = gui.getViewportSize().y() - y * gui.getViewportSize().y() - button_half_size;

				currentTargetButton->setPosition(xx, yy);
			}
		}
	}
}

void glFace4Blending::initializeGUI (int width, int height)
{
	gui.setViewportSize(width, height);

	saveButton.setPosition(button_size, button_size);
	saveButton.onClick ([&]() {onSaveButtonPressed(); });
	saveButton.setTexture (std::string(TUCANO_ASSETS_DIR) + "/save.png");
	saveButton.setDimensionsFromHeight(button_size);
	gui.add(&saveButton);

	screenshotButton.setPosition(button_size * 2.2, button_size);
	screenshotButton.onClick ([&]() {onScreenshotButtonPressed(); });
	screenshotButton.setTexture (std::string(TUCANO_ASSETS_DIR) + "/screenshot.png");
	screenshotButton.setDimensionsFromHeight(button_size);
	gui.add(&screenshotButton);


	auto thumbnails = project.getThumbnails();
	auto thumbnails_count = thumbnails.size();

#if 1
	// texture buttons
	{
		textureCheckbox = std::vector<Tucano::GUI::SelectButton>(thumbnails_count);
		const int position_y = button_size * 2 + button_half_size;
		for (auto i = 0; i < thumbnails_count; ++i)
		{
			textureCheckbox[i].setPosition(button_size * (i + 1) + button_half_size * i, position_y);
			textureCheckbox[i].setColor({ 0.7f, 0.7f, 0.7f, 1.f });
			textureCheckbox[i].setTexture(std::string(TUCANO_ASSETS_DIR) + "/unchecked_checkbox.png");
			textureCheckbox[i].setAltTexture(std::string(TUCANO_ASSETS_DIR) + "/checked_checkbox.png");
			textureCheckbox[i].onClick ([&, i]() {onTextureCheckboxClicked(i); });
			textureCheckbox[i].setSelected(true);
			textureCheckbox[i].setDimensionsFromHeight(button_size * 2);
			gui.add(&textureCheckbox[i]);
		}
		for (auto i = 0; i < thumbnails_count; ++i)
		{
			textureCheckbox[i].setTexture(thumbnails[i].filename);
			textureCheckbox[i].setAltTexture(thumbnails[i].filename);
		}
	}
#endif

	// sliders
	{
		int position_y = button_size * 5 + button_half_size;
		const int slide_width = 160;
		const int slide_height = 32;
		int index = 0;

		for (auto& slider : maskSlider)
		{
			slider.setPosition(button_size, position_y);
			slider.setDimensions(slide_width, slide_height);
			slider.onValueChanged([&, index](float v) 
				{
					std::cout << v << std::endl;
					blending.setMaskWeight(index, v); 
				});
			slider.setTexture(std::string(TUCANO_ASSETS_DIR) + "/slider_bar.png", std::string(TUCANO_ASSETS_DIR) + "/slider.png");
			slider.setMinMaxValues(0.0, 1.0);
			slider.moveSlider(blending.getMaskWeight(index));
			gui.add(&slider);
			position_y += button_size;
			index++;
		}

		position_y += button_size;
		displacementSlider.setPosition(button_size, position_y);
		displacementSlider.setDimensions(slide_width, slide_height);
		displacementSlider.onValueChanged([&, index](float v) {blending.setDisplacementMultiplier(v); });
		displacementSlider.setTexture(std::string(TUCANO_ASSETS_DIR) + "/slider_bar.png", std::string(TUCANO_ASSETS_DIR) + "/slider.png");
		displacementSlider.setMinMaxValues(0.5, 1.5);
		displacementSlider.moveSlider(blending.getDisplacementMultiplier());
		gui.add(&displacementSlider);


		//position_y += button_size;
		//normalVertFaceLerpSlider.setPosition(button_size, position_y);
		//normalVertFaceLerpSlider.setDimensions(slide_width, slide_height);
		//normalVertFaceLerpSlider.onValueChanged([&, index](float v) {blending.setNormalVertFaceLerp(v); });
		//normalVertFaceLerpSlider.setTexture(std::string(TUCANO_ASSETS_DIR) + "/slider_bar.png", std::string(TUCANO_ASSETS_DIR) + "/slider.png");
		//normalVertFaceLerpSlider.setMinMaxValues(0.0f, 1.0f);
		//normalVertFaceLerpSlider.moveSlider(blending.getNormalVertFaceLerp());
		//gui.add(&normalVertFaceLerpSlider);


		index = 0;
		for (auto& slider : colorDiffSlider)
		{
			position_y += button_size;
			slider.setPosition(button_size, position_y);
			slider.setDimensions(slide_width, slide_height);
			slider.onValueChanged([&, index](float v) {blending.setColorDiffMultiplier(v, index); });
			slider.setTexture(std::string(TUCANO_ASSETS_DIR) + "/slider_bar.png", std::string(TUCANO_ASSETS_DIR) + "/slider.png");
			slider.setMinMaxValues(0.0, 8.0);
			slider.moveSlider(blending.getColorDiffMultiplier(index));
			gui.add(&slider);
			index++;
		}
	}

	

	// thumbnails
	thumbnail = std::vector<Tucano::GUI::Button>(thumbnails_count);
	{
		for (auto i = 0; i < thumbnails_count; ++i)
		{
			thumbnail[i].onClick ([&, i]() {onButtonPressed(i); });
			thumbnail[i].onhover([&]() {});
			thumbnail[i].onRelease([&]() {});
			thumbnail[i].setTexture(thumbnails[0].filename);
			thumbnail[i].setDimensionsFromHeight(128);
			gui.add(&thumbnail[i]);
		}

		auto w = (width - thumb_size);
		auto h = (height - thumb_size - button_size);
		for (auto i = 0; i < thumbnails_count; ++i)
		{
			auto x = (thumbnails[i].x * 0.5f + 0.5f) * w;
			auto y = h - (thumbnails[i].y * 0.5f + 0.5f) * h;

			thumbnail[i].setPosition(x, y);
			thumbnail[i].setTexture(thumbnails[i].filename);
		}
	}

	//
	// Target buttons
	//
	{
		for (auto i = 0; i < targetButton.size(); ++i)
		{
			targetButton[i].setPosition(i * button_size + viewportSize.x() * 0.5, viewportSize.y() * 0.5);
			targetButton[i].setTexture (std::string(TUCANO_ASSETS_DIR) + "/target.png");
			targetButton[i].setDimensionsFromHeight(button_size);
			targetButton[i].onClick ([&, i]() {onTargetButtonPressed(i); });
			gui.add(&targetButton[i]);
		}
		targetButton[0].setColor({ 1.0, 0.5, 0.5, 1 });
		targetButton[1].setColor({ 0.5, 1.0, 0.5, 1 });
		targetButton[2].setColor({ 0.5, 0.5, 1.0, 1 });
		currentTargetButton = &targetButton[0];
	}

	selectionButton.setPosition(thumbnail[0].getPosition() - Eigen::Vector2i(0, button_size));
	selectionButton.setTexture (std::string(TUCANO_ASSETS_DIR) + "/circle.png");
	selectionButton.setDimensions(button_size * 4, button_size * 6);
	gui.add(&selectionButton);
}

void glFace4Blending::initialize (int width, int height, const char* projectfilepath)
{
	if (!project.load(projectfilepath))
	{
		std::cerr << "Error: Could not load project file " << projectfilepath << std::endl;
		return;
	}

	viewportSize = { width, height };

	initializeGUI(width, height);

    // initialize the shader effect
	blending.initialize();
	blending.setShininessCoeff(10.0f);
	blending.enableTexture(false);
	
	camera.setPerspectiveMatrix(35.0, width/(float)height, 0.1f, 100.0f);
	camera.setViewport(Eigen::Vector2f ((float)width, (float)height));

	setRenderMode(ERenderMode::ALL_FACES);

	loadMeshes();

	blending.setTexture(texture[0]);
	blending.setMaskTexture(maskTexture);
	
	Tucano::Misc::errorCheckFunc(__FILE__, __LINE__);

    glEnable(GL_DEPTH_TEST);

	// base mesh is the young bent face
	blending.setBaseMesh(0);

	pbo.create();

	////////////////////////////////////////////////////
	//
	// Creating ssbo
	//
	std::vector<float> ssbo_data;
	for (const auto& v : vertOriginal)
		std::copy(v.begin(), v.end(), std::back_inserter(ssbo_data));
	for (const auto& v : vertSmoothed)
		std::copy(v.begin(), v.end(), std::back_inserter(ssbo_data));
	for (const auto& v : polyfit)
		std::copy(v.begin(), v.end(), std::back_inserter(ssbo_data));
	//for (const auto& v : rgb)
	//	std::copy(v.begin(), v.end(), std::back_inserter(ssbo_data));
	//
	ssbo = Tucano::ShaderStorageBufferFloat(ssbo_data.size());
	ssbo.initialize();
	ssbo.bind();
	glBufferData(ssbo.getBufferType(), ssbo.getSizeInBytes(), ssbo_data.data(), GL_STATIC_DRAW);
	ssbo.unbind();
	////////////////////////////////////////////////////
}

void glFace4Blending::paintGL (void)
{
   	camera.updateViewMatrix();
	
 	glClearColor(0.2, 0.2, 0.2, 0.0);
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

	mesh.modelMatrix()->setIdentity();

	ssbo.bindBase(0);
	blending.render(mesh, camera, light);
	ssbo.unbindBase();

	if (pbo.is_created())
		pbo.process();

	if (renderGUIEnabled)
		gui.render();

	//camera.renderAtCorner();
}

static float frame_counter = 0;
void glFace4Blending::update()
{
	for (auto i = 0; i < thumbnail.size(); ++i)
		blending.pos2d[i] = Eigen::Vector2f(thumbnail[i].getPosition().x() / viewportSize.x(), 1 - (thumbnail[i].getPosition().y() / viewportSize.y())) * 2 - Eigen::Vector2f(1,1);
}


void glFace4Blending::saveScreenshot(const char* filename)
{
	pbo.save_image(filename);
}


void glFace4Blending::loadMeshes()
{
	std::vector<std::string> originalMeshes = project.getOriginalMeshesFilename();
	std::vector<std::string> smoothedMeshes = project.getSmoothedMeshesFilename();
	std::vector<std::string> texturesFilename = project.getTexturesFilename();
	std::vector<std::string> polyfitFilename = project.getPolyfitFilename();
	

	timer t_loading;
	t_loading.start();

	texture = std::vector<Tucano::Texture>(texturesFilename.size());
#if 0
	// initialize texture with given image
	for (int i = 0; i < texturesFilename.size(); ++i)
	{
		std::cout << "Loading: " << texturesFilename[i] << std::endl;
		Tucano::ImageImporter::loadImage(texturesFilename[i], &texture[i], true);
	}
#endif
	std::cout << "Loading: " << project.getMaskTexFilename() << std::endl;
	Tucano::ImageImporter::loadImage(project.getMaskTexFilename(), &maskTexture, true, maskTexData, maskTexWidth, maskTexHeight, maskTexChannels);

	t_loading.stop();
	t_loading.print_interval("-- Loading images : ");

	if (originalMeshes.size() != smoothedMeshes.size())
	{
		std::cerr << "Error: Number of meshes original and smoothed does not match. Abort" << std::endl;
		return;
	}

	const auto mesh_count = originalMeshes.size();
	
	vertOriginal = std::vector<std::vector<float>>(mesh_count);
	vertSmoothed = std::vector<std::vector<float>>(mesh_count);
	polyfit      = std::vector<std::vector<float>>(mesh_count);

	t_loading.start();
	std::cout << "Loading: " << originalMeshes[0] << std::endl;
	auto vert_count_o = vector_read(originalMeshes[0], vertOriginal[0]);
	std::cout << "Loading: " << smoothedMeshes[0] << std::endl;
	auto vert_count_s = vector_read(smoothedMeshes[0], vertSmoothed[0]);
	std::cout << "Loading: " << polyfitFilename[0] << std::endl;
	auto polyfit_count = vector_read(polyfitFilename[0], polyfit[0]);

	if (vert_count_o != vert_count_s)
	{
		std::cerr << "Error: Number of vertices does not match. Abort" << std::endl;
		return;
	}
	const auto vert_count = vert_count_o;
	for (auto i = 1; i < originalMeshes.size(); ++i)
	{
		std::cout << "Loading: " << originalMeshes[i] << std::endl;
		vert_count_o = vector_read(originalMeshes[i], vertOriginal[i]);
		std::cout << "Loading: " << smoothedMeshes[i] << std::endl;
		vert_count_s = vector_read(smoothedMeshes[i], vertSmoothed[i]);
		std::cout << "Loading: " << polyfitFilename[i] << std::endl;
		polyfit_count = vector_read(polyfitFilename[i], polyfit[i]);

		if (vert_count_o != vert_count_s || vert_count_s != vert_count || vert_count_o != vert_count)
		{
			std::cerr << "Error: Number of vertices does not match. Abort" << std::endl;
			return;
		}
	}

	auto norm_count = vector_read(project.getNormalMeshFilename(), normals);
	auto uv_count = vector_read(project.getUVMeshFilename(), texcoords);
	auto tris_count = vector_read(project.getTriMeshFilename(), indices);
	//auto rgb_count = vector_read(project.getRGBMeshFilename(), rgb);

	t_loading.stop();
	t_loading.print_interval("-- Loading mesh files: ");

	std::cout << "Data Loaded: \n"
		<< vert_count << " vertices \n"
		<< norm_count << " normals : " << project.getNormalMeshFilename() << std::endl
		<< uv_count << " texcoords : " << project.getUVMeshFilename() << std::endl
		//<< rgb_count << " rgb : " << project.getRGBMeshFilename() << std::endl
		<< tris_count / 3 << " triangles : " << project.getTriMeshFilename() << std::endl;
	
	t_loading.start();

	std::vector<Eigen::Vector4f> vert4f(vert_count / 3);

	for (auto i = 0; i < vert4f.size(); ++i)
	{
		vert4f[i] =
		{
			vertOriginal[0][i * 3 + 0],
			vertOriginal[0][i * 3 + 1],
			vertOriginal[0][i * 3 + 2],
			1
		};
	}

	// load attributes found in file
	mesh.loadVertices(vert4f);
	mesh.loadNormals(normals);
	mesh.loadTexCoords(texcoords);
	mesh.loadIndices(indices);
	

	blending.setMeshCount(mesh_count);
	
	//mesh.createAttribute("in_VertexSmoothed_0", verticesSmoothed[0]);
	//for (auto i = 1; i < mesh_count; ++i)
	//{
	//	mesh.createAttribute("in_VertexOriginal_" + std::to_string(i), verticesOriginal[i]);
	//	mesh.createAttribute("in_VertexSmoothed_" + std::to_string(i), verticesSmoothed[i]);
	//}

	// sets the default locations for accessing attributes in shaders
	mesh.setDefaultAttribLocations();
	mesh.normalizeModelMatrix();

	t_loading.stop();
	t_loading.print_interval("-- Configuring mesh: ");
}



bool glFace4Blending::saveMesh(const std::string& filename, bool invert_face)	// for meshlab, invert_face = true
{
	timer t;
	t.start();

	std::cout << "save: " << filename << std::endl;

	// update project file
	{
		project.setMousePosition("mouse_pos_0_x", blending.mouse_pos[0].x());
		project.setMousePosition("mouse_pos_0_y", blending.mouse_pos[0].x());
		project.setMousePosition("mouse_pos_1_x", blending.mouse_pos[1].x());
		project.setMousePosition("mouse_pos_1_y", blending.mouse_pos[1].x());
		project.setMousePosition("mouse_pos_2_x", blending.mouse_pos[2].x());
		project.setMousePosition("mouse_pos_2_y", blending.mouse_pos[2].x());

		project.setMousePosition("mask_weight_x", blending.getMaskWeight(0));
		project.setMousePosition("mask_weight_y", blending.getMaskWeight(1));
		project.setMousePosition("mask_weight_z", blending.getMaskWeight(2));

		project.setCurveInterpolation("curve_interpolation", blending.useCurveInterpolation());
		
	}

	const float epsilon = 0.001f;
	const float pow_distance = 2.0f;
	const int coeff_order = 3 + 1;

	Eigen::Vector2f mouse_screen[3] =
	{
		Eigen::Vector2f(blending.mouse_pos[0].x() * 2 - 1, blending.mouse_pos[0].y() * 2 - 1),
		Eigen::Vector2f(blending.mouse_pos[1].x() * 2 - 1, blending.mouse_pos[1].y() * 2 - 1),
		Eigen::Vector2f(blending.mouse_pos[2].x() * 2 - 1, blending.mouse_pos[2].y() * 2 - 1)
	};

	const auto mesh_count = vertOriginal.size();
	const auto n_verts = vertOriginal[0].size() / 3;
	std::vector<float> vertex_output = vertOriginal[0];
	//std::vector<float> rgb_output(vertOriginal[0].size(), 0);

	//
	// for each vertex
	//
	for (auto iv = 0; iv < n_verts; ++iv)
	{
		Eigen::Vector4f mask_tex = Tucano::ImageImporter::getPixel(
			maskTexData, maskTexWidth, maskTexHeight, maskTexChannels, 
			texcoords[iv].x() * maskTexWidth, texcoords[iv].y() * maskTexHeight);

		float smooth_mix = (mask_tex[0] * blending.getMaskWeight(0)) + (mask_tex[1] * blending.getMaskWeight(1)) + (mask_tex[2] * blending.getMaskWeight(2));


		//rgb_output[iv * 3 + 0] = mask_tex[0] * 255;
		//rgb_output[iv * 3 + 1] = mask_tex[1] * 255;
		//rgb_output[iv * 3 + 2] = mask_tex[2] * 255;
	
		std::vector<Eigen::Vector3f> d_lerp(mesh_count, Eigen::Vector3f::Zero());
		std::vector<Eigen::Vector3f> d_curve(mesh_count, Eigen::Vector3f::Zero());
		std::vector<Eigen::Vector3f> w(mesh_count, Eigen::Vector3f::Zero());

		//
		// for each mesh
		//
		for (auto im = 0; im < mesh_count; ++im)
		{
			d_lerp[im] = get_vertex_from_mesh_original(im, iv) - get_vertex_from_mesh_smoothed(im, iv);

			d_curve[im] = get_vertex_curve_fit(im, iv, 0) - get_vertex_curve_fit(im, iv, smooth_mix);

			const Eigen::Vector2f pos2d = blending.pos2d[im];

			Eigen::Vector3f dist = Eigen::Vector3f(
				(mouse_screen[0] - pos2d).norm(),
				(mouse_screen[1] - pos2d).norm(),
				(mouse_screen[2] - pos2d).norm());


			w[im][0] = pow(1.0f / (dist[0] + epsilon), pow_distance);
			w[im][1] = pow(1.0f / (dist[1] + epsilon), pow_distance);
			w[im][2] = pow(1.0f / (dist[2] + epsilon), pow_distance);
		}

		Eigen::Vector3f sum_w(0, 0, 0);
		for (int i = 0; i < mesh_count; ++i)
			sum_w += w[i];

		//
		// computing vertex base
		//
		Eigen::Vector3f vbase_lerp = Eigen::Vector3f(0);
		{
			Eigen::Vector3f v_smoothed = get_vertex_from_mesh_smoothed(blending.getBaseMesh(), iv);
			vbase_lerp = v_smoothed + d_lerp[blending.getBaseMesh()] * (1 - smooth_mix);
		}

		Eigen::Vector3f vbase_curve = get_vertex_curve_fit(blending.getBaseMesh(), iv, smooth_mix);


		Eigen::Vector3f disp_masked_lerp[3] = { Eigen::Vector3f::Zero(), Eigen::Vector3f::Zero(), Eigen::Vector3f::Zero() };
		Eigen::Vector3f disp_masked_curve[3] = { Eigen::Vector3f::Zero(), Eigen::Vector3f::Zero(), Eigen::Vector3f::Zero() };

		for (auto im = 0; im < mesh_count; ++im)
		{
			disp_masked_lerp[0] = disp_masked_lerp[0] + w[im][0] * mask_tex.x() * smooth_mix * d_lerp[im];
			disp_masked_lerp[1] = disp_masked_lerp[1] + w[im][1] * mask_tex.y() * smooth_mix * d_lerp[im];
			disp_masked_lerp[2] = disp_masked_lerp[2] + w[im][2] * mask_tex.z() * smooth_mix * d_lerp[im];

			disp_masked_curve[0] = disp_masked_curve[0] + w[im][0] * mask_tex.x() * d_curve[im];
			disp_masked_curve[1] = disp_masked_curve[1] + w[im][1] * mask_tex.y() * d_curve[im];
			disp_masked_curve[2] = disp_masked_curve[2] + w[im][2] * mask_tex.z() * d_curve[im];
		}


		disp_masked_lerp[0] = disp_masked_lerp[0] / sum_w[0];
		disp_masked_lerp[1] = disp_masked_lerp[1] / sum_w[1];
		disp_masked_lerp[2] = disp_masked_lerp[2] / sum_w[2];

		disp_masked_curve[0] = disp_masked_curve[0] / sum_w[0];
		disp_masked_curve[1] = disp_masked_curve[1] / sum_w[1];
		disp_masked_curve[2] = disp_masked_curve[2] / sum_w[2];

		Eigen::Vector3f disp_lerp = Eigen::Vector3f(disp_masked_lerp[0] + disp_masked_lerp[1] + disp_masked_lerp[2]);
		Eigen::Vector3f disp_curve = Eigen::Vector3f(disp_masked_curve[0] + disp_masked_curve[1] + disp_masked_curve[2]);

		Eigen::Vector3f in_Vertex_lerp = Eigen::Vector3f(vbase_lerp + disp_lerp * blending.getDisplacementMultiplier());
		Eigen::Vector3f in_Vertex_curve = Eigen::Vector3f(vbase_curve + disp_curve * blending.getDisplacementMultiplier());

		if (blending.useCurveInterpolation())
		{
			vertex_output[iv * 3 + 0] = in_Vertex_curve.x();
			vertex_output[iv * 3 + 1] = in_Vertex_curve.y();
			vertex_output[iv * 3 + 2] = in_Vertex_curve.z();
		}
		else
		{
			vertex_output[iv * 3 + 0] = in_Vertex_lerp.x();
			vertex_output[iv * 3 + 1] = in_Vertex_lerp.y();
			vertex_output[iv * 3 + 2] = in_Vertex_lerp.z();
		}
	}
	std::cout << "[Info] Vertex processing " << t.diff_sec_now() << "s" << std::endl;


	//
	// Output obj file
	//
	bool success_saving = false;
	{
		tinyobj::scene_t output_obj;
		output_obj.attrib.vertices = vertex_output;
		//output_obj.attrib.colors = rgb_output;
		tinyobj::shape_t shape;

		if (invert_face)
		{
			for (auto elem = indices.rbegin(); elem != indices.rend(); ++elem)
				shape.mesh.indices.push_back({ (int)*elem, 0, 0 });
		}
		else
		{
			for (const auto& ind : indices)
				shape.mesh.indices.push_back({ (int)ind, 0, 0 });
		}

		shape.mesh.num_face_vertices = std::vector<uint8_t>(shape.mesh.indices.size() / 3, 3);
		shape.name = "main_shape";
		output_obj.shapes.push_back(shape);

		std::cout << "[Info] Saving obj file : " << filename << std::endl;
		success_saving = tinyobj::save(output_obj, filename);
		if (!success_saving)
			std::cerr << "[Error] Saving obj file : " << filename << std::endl;
		else
			std::cout << "[Info] Saved obj file  : " << filename << std::endl;
	}

	std::cout << "[Info] Finished " << t.diff_sec_now() << "s" << std::endl;

	return success_saving;
}



Eigen::Vector3f glFace4Blending::get_vertex_from_mesh_original(std::size_t mesh_index, std::size_t vert_index) const
{
	return Eigen::Vector3f(
		vertOriginal[mesh_index][vert_index * 3 + 0],
		vertOriginal[mesh_index][vert_index * 3 + 1],
		vertOriginal[mesh_index][vert_index * 3 + 2]);
}

Eigen::Vector3f glFace4Blending::get_vertex_from_mesh_smoothed(std::size_t mesh_index, std::size_t vert_index) const
{
	return Eigen::Vector3f(
		vertSmoothed[mesh_index][vert_index * 3 + 0],
		vertSmoothed[mesh_index][vert_index * 3 + 1],
		vertSmoothed[mesh_index][vert_index * 3 + 2]);
}

Eigen::Vector3f glFace4Blending::get_vertex_curve_fit(std::size_t mesh_index, std::size_t vert_index, float mix) const
{
	const int coeff_order = 3 + 1;
	const auto n_verts = vertOriginal[0].size() / 3;
	const auto& mesh = polyfit[mesh_index];

	auto ivx = (vert_index * coeff_order) + (n_verts * 0 * coeff_order);
	auto ivy = (vert_index * coeff_order) + (n_verts * 1 * coeff_order);
	auto ivz = (vert_index * coeff_order) + (n_verts * 2 * coeff_order);

	Eigen::Vector4f coeff_x(mesh[ivx + 0], mesh[ivx + 1], mesh[ivx + 2], mesh[ivx + 3]);
	Eigen::Vector4f coeff_y(mesh[ivy + 0], mesh[ivy + 1], mesh[ivy + 2], mesh[ivy + 3]);
	Eigen::Vector4f coeff_z(mesh[ivz + 0], mesh[ivz + 1], mesh[ivz + 2], mesh[ivz + 3]);

	return Eigen::Vector3f(
		coeff_x.w() * pow(mix, 3) + coeff_x.z() * pow(mix, 2) + coeff_x.y() * mix + coeff_x.x(),
		coeff_y.w() * pow(mix, 3) + coeff_y.z() * pow(mix, 2) + coeff_y.y() * mix + coeff_y.x(),
		coeff_z.w() * pow(mix, 3) + coeff_z.z() * pow(mix, 2) + coeff_z.y() * mix + coeff_z.x()
	);
}



