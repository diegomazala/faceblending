#include <iostream>
#include "glface4blending.hpp"
#include "GLFW/glfw3.h"

static int gWindowWidth = 1920;
static int gWindowHeight = 1080;
static bool gPrintFps = false;

glFace4Blending *glScene;

void initialize (const char* projectfilepath)
{
    Tucano::Misc::initGlew();
	glScene = new glFace4Blending();
	glScene->initialize(gWindowWidth, gWindowHeight, projectfilepath);

    cout << endl << endl << " ************ usage ************** " << endl;
    cout << " R : reset camera" << endl;
    cout << "ASWD : move Left, Rigth, Forward, Back" << endl;
    cout << "EC : move Up, Down" << endl;
    cout << "K : add key point" << endl;
    cout << "SPACE : start/stop camera animation" << endl;
    cout << ",. : step forward/backward in animation" << endl;
    cout << "0 : toggle draw control points" << endl;
    cout << "Z-X : shininess" << endl;
	cout << "T : texture on/off" << endl;
    cout << " ***************** " << endl;
}

void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, 1);	
	if (key == GLFW_KEY_R && action == GLFW_PRESS)
		glScene->getCamera()->reset();
	if (key == GLFW_KEY_A && (action == GLFW_PRESS || action == GLFW_REPEAT))
		glScene->getCamera()->strideLeft();
	if (key == GLFW_KEY_D && (action == GLFW_PRESS || action == GLFW_REPEAT))
		glScene->getCamera()->strideRight();
	if (key == GLFW_KEY_S && (action == GLFW_PRESS || action == GLFW_REPEAT))
		glScene->getCamera()->moveBack();
	if (key == GLFW_KEY_W && (action == GLFW_PRESS || action == GLFW_REPEAT))
		glScene->getCamera()->moveForward();
 	if (key == GLFW_KEY_Q && (action == GLFW_PRESS || action == GLFW_REPEAT))
		glScene->getCamera()->moveDown();
	if (key == GLFW_KEY_E && (action == GLFW_PRESS || action == GLFW_REPEAT))
		glScene->getCamera()->moveUp();	
	if (key == GLFW_KEY_PRINT_SCREEN && (action == GLFW_PRESS || action == GLFW_REPEAT))
		glScene->onScreenshotButtonPressed();

	if (key == GLFW_KEY_B && (action == GLFW_PRESS || action == GLFW_REPEAT))
		glScene->onScreenshotButtonPressed();

	if (key == GLFW_KEY_DELETE && (action == GLFW_PRESS || action == GLFW_REPEAT))
		glScene->getMesh()->modelMatrix()->translate(Eigen::Vector3f(-1, 0, 0));
	if (key == GLFW_KEY_PAGE_DOWN && (action == GLFW_PRESS || action == GLFW_REPEAT))
		glScene->getMesh()->modelMatrix()->translate(Eigen::Vector3f(1, 0, 0));
	if (key == GLFW_KEY_HOME && (action == GLFW_PRESS || action == GLFW_REPEAT))
		glScene->getMesh()->modelMatrix()->translate(Eigen::Vector3f(0, 0, -1));
	if (key == GLFW_KEY_END && (action == GLFW_PRESS || action == GLFW_REPEAT))
		glScene->getMesh()->modelMatrix()->translate(Eigen::Vector3f(0, 0, 1));

	if (key == GLFW_KEY_Z && (action == GLFW_PRESS || action == GLFW_REPEAT))
	{
		glScene->getBlendingEffect()->setShininessCoeff(glScene->getBlendingEffect()->getShininessCoeff() - 1);
		std::cout << "-- Shininess : " << glScene->getBlendingEffect()->getShininessCoeff() << std::endl;
	}
	if (key == GLFW_KEY_X && (action == GLFW_PRESS || action == GLFW_REPEAT))
	{
		glScene->getBlendingEffect()->setShininessCoeff(glScene->getBlendingEffect()->getShininessCoeff() + 1);
		std::cout << "-- Shininess : " << glScene->getBlendingEffect()->getShininessCoeff() << std::endl;
	}

	if (key == GLFW_KEY_T && (action == GLFW_PRESS || action == GLFW_REPEAT))
	{
		glScene->getBlendingEffect()->enableTexture(!glScene->getBlendingEffect()->isTextureEnabled());
		std::cout << "-- Texture : " << glScene->getBlendingEffect()->isTextureEnabled() << std::endl;
	}

	if (key == GLFW_KEY_I && (action == GLFW_PRESS || action == GLFW_REPEAT))
	{
		glScene->getBlendingEffect()->invertTextureVertically(!glScene->getBlendingEffect()->invertTextureVertically());
		std::cout << "-- Texture Invert Y : " << glScene->getBlendingEffect()->invertTextureVertically() << std::endl;
	}

	if (key == GLFW_KEY_M && (action == GLFW_PRESS || action == GLFW_REPEAT))
	{
		glScene->getBlendingEffect()->useMaskColor(!glScene->getBlendingEffect()->useMaskColor());
		std::cout << "-- Mask : " << glScene->getBlendingEffect()->useMaskColor() << std::endl;
	}

	if (key == GLFW_KEY_L && (action == GLFW_PRESS || action == GLFW_REPEAT))
	{
		glScene->getBlendingEffect()->useCurveInterpolation(!glScene->getBlendingEffect()->useCurveInterpolation());
		std::cout << "-- Curve Interpolation : " << glScene->getBlendingEffect()->useCurveInterpolation() << std::endl;
	}

	if (key == GLFW_KEY_G && (action == GLFW_PRESS || action == GLFW_REPEAT))
	{
		glScene->enableRenderGUI(!glScene->isRenderGUIEnabled());
		std::cout << "-- RenderGUI : " << glScene->isRenderGUIEnabled() << std::endl;
	}

	
	if (key == GLFW_KEY_F && (action == GLFW_PRESS || action == GLFW_REPEAT))
	{
		gPrintFps = !gPrintFps;
	}

	if (key == GLFW_KEY_SPACE && (action == GLFW_PRESS || action == GLFW_REPEAT))
	{
		glScene->enableAnim(!glScene->isAnimEnabled());
		std::cout << "-- Animation : " << glScene->isAnimEnabled() << std::endl;
	}

	if (key == GLFW_KEY_N && (action == GLFW_PRESS || action == GLFW_REPEAT))
	{
		glScene->getBlendingEffect()->setNormalVertFaceLerp(!(bool)(int)glScene->getBlendingEffect()->getNormalVertFaceLerp());
		glScene->setNormalVertFaceLerp(glScene->getBlendingEffect()->getNormalVertFaceLerp());
		std::cout << "-- Face normal: " << glScene->getBlendingEffect()->getNormalVertFaceLerp() << std::endl;
	}

	for (auto num_key = GLFW_KEY_F1; num_key <= GLFW_KEY_F4; ++num_key)
	{
		if (key == num_key && (action == GLFW_PRESS || action == GLFW_REPEAT))
		{
			glScene->setRenderMode(static_cast<glFace4Blending::ERenderMode>(num_key - GLFW_KEY_F1));
			std::cout << "-- Set render mode: " << num_key - GLFW_KEY_F1 << std::endl;
		}
	}

	for (auto num_key = GLFW_KEY_KP_0; num_key < GLFW_KEY_KP_4; ++num_key)
	{
		if (key == num_key && (action == GLFW_PRESS || action == GLFW_REPEAT))
		{
			glScene->getBlendingEffect()->setBaseMesh(num_key - GLFW_KEY_KP_0);
			std::cout << "-- Set base mesh: " << num_key - GLFW_KEY_KP_0 << std::endl;
		}
	}
}

static void mouseButtonCallback (GLFWwindow* window, int button, int action, int mods)
{
	bool left_ctrl_pressed = glfwGetKey(window, GLFW_KEY_LEFT_CONTROL) == GLFW_PRESS;
	bool left_alt_pressed = glfwGetKey(window, GLFW_KEY_LEFT_ALT) == GLFW_PRESS;
	bool left_shift_pressed = glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS;
	glScene->keyModifierPressed(left_ctrl_pressed, left_shift_pressed, left_alt_pressed);

	double xpos, ypos;
	glfwGetCursorPos(window, &xpos, &ypos);

    if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS)
	{
		glScene->getCamera()->startRotation( Eigen::Vector2f (xpos, ypos) );
		if (glScene->getGUI()->leftButtonPressed (xpos, ypos))
			return;
	}

	if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_RELEASE)
	{
		glScene->getGUI()->leftButtonReleased (xpos, ypos);
	}

}

static void cursorPosCallback(GLFWwindow* window, double xpos, double ypos)
{
	bool left_ctrl_pressed = glfwGetKey(window, GLFW_KEY_LEFT_CONTROL) == GLFW_PRESS;
	bool left_alt_pressed = glfwGetKey(window, GLFW_KEY_LEFT_ALT) == GLFW_PRESS;
	bool left_shift_pressed = glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS;

	glScene->keyModifierPressed(left_ctrl_pressed, left_shift_pressed, left_alt_pressed);
	glScene->setMousePos(xpos, ypos);

	if (left_ctrl_pressed && glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_1) == GLFW_PRESS)
	{
		glScene->getCamera()->rotate(Eigen::Vector2f(xpos, ypos));
	}

	if (glScene)
		glScene->onMouseMove(xpos, ypos);

	if (glScene->getGUI()->cursorMove (xpos, ypos))
		return;
}


int main(int argc, char *argv[])
{
	GLFWwindow* main_window;

	if (!glfwInit()) 
	{
    	std::cerr << "Failed to init glfw" << std::endl;
		return 1;
	}

	bool fullscreen = false;

	GLFWmonitor* monitor = glfwGetPrimaryMonitor();
	const GLFWvidmode* mode = glfwGetVideoMode(monitor);
	glfwWindowHint(GLFW_RED_BITS, mode->redBits);
	glfwWindowHint(GLFW_GREEN_BITS, mode->greenBits);
	glfwWindowHint(GLFW_BLUE_BITS, mode->blueBits);
	glfwWindowHint(GLFW_REFRESH_RATE, mode->refreshRate);
	gWindowWidth = mode->width;
	gWindowHeight = mode->height - (fullscreen ? 0 : 64);
	main_window = glfwCreateWindow(gWindowWidth, gWindowHeight, "TUCANO :: Face Blending", (fullscreen ? monitor : NULL), NULL);
	if (!main_window)
	{
		std::cerr << "Failed to create the GLFW window" << std::endl;
		glfwTerminate();
		return -1;
	}

	glfwMakeContextCurrent(main_window);
	glfwSetKeyCallback(main_window, keyCallback);
	glfwSetMouseButtonCallback(main_window, mouseButtonCallback);
	glfwSetCursorPosCallback(main_window, cursorPosCallback);
	glfwSetWindowPos(main_window, 0, 32);

	glfwSetInputMode(main_window, GLFW_STICKY_KEYS, true);

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
   	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
   	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	std::cout << "OpenGL: " << glGetString(GL_VERSION) << std::endl;

	std::string projectfilepath = (argc > 1) ? argv[1] : std::string(TUCANO_MODELS_DIR) + "/figs.prj";
	initialize(projectfilepath.c_str());

	double lastTime = glfwGetTime();
	int nbFrames = 0;

	while (!glfwWindowShouldClose(main_window))
	{
		glfwMakeContextCurrent(main_window);
		
		glScene->update();
		glScene->paintGL();



		// Measure speed
		double currentTime = glfwGetTime();
		nbFrames++;

		if (currentTime - lastTime >= 1.0 && gPrintFps)
		{ 
			// If last prinf() was more than 1 sec ago
			// printf and reset timer
			printf("%f ms/frame\n", 1000.0 / double(nbFrames));
			nbFrames = 0;
			lastTime += 1.0;
		}

		glfwSwapBuffers( main_window );

		glfwPollEvents();
	}

	glfwDestroyWindow(main_window);
	glfwTerminate();
	return 0;
}
